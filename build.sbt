name := "Tress"

version := "0.1"

scalaVersion := "2.13.3"

scalacOptions ++= Seq("-Ylog-classpath")

resolvers += "jitpack" at "https://jitpack.io"

val http4sVersion = "0.21.5"

/* Configuration file handling */
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.16.0"

/* API and json */
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.2"
libraryDependencies += "com.lihaoyi" %% "requests" % "0.6.5"

/* LOGGING */
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3"

/* Web server */
libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion
)

/* Twitch IRC */
libraryDependencies += "com.github.Gikkman" % "Java-Twirk" % "0.7.1"

/* JavaFX */
libraryDependencies += "org.scalafx" %% "scalafx" % "12.0.2-R18"
libraryDependencies += "org.controlsfx" % "controlsfx" % "11.0.0"
libraryDependencies += "de.jensd" % "fontawesomefx" % "8.9"

/* MISC */
libraryDependencies += "joda-time" % "joda-time" % "2.10.10"

// JavaFX config

fork := true

lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux") => "linux"
  case n if n.startsWith("Mac") => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}

lazy val javaFXModules = Seq("base", "controls", "graphics")
libraryDependencies ++= javaFXModules.map( m=>
  "org.openjfx" % s"javafx-$m" % "11" classifier osName
)

// sbt assembly

assemblyMergeStrategy in assembly := {
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

// disable using the Scala version in output paths and artifacts
crossPaths := false

assemblyJarName in assembly := "Tress.jar"