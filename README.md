# Tress

_A simple Twitch client for those who just want to watch 
their favorites streams._

![screenshot](.gitlab/screenshot.png)

**WARNING**: This is a WIP, things might break. 
If you encounter a bug or want to suggest a new feature, 
feel free to open an issue.

## Known issues

- Clicking "Load more" loads streams/games that are
  already being displayed when reaching the end of a list. This is an issue
  with the API and can't be fixed for now.
- User icon takes some times to disappear when logging off.
- Twitch animated emotes are static (no toggle yet)

## Requirements

In order to run Tress, you'll need the following requirements:

- Java 11 or higher

To be able to watch streams, you'll need:

- [StreamLink](https://streamlink.github.io/)
- A media player [supported by streamlink](https://streamlink.github.io/players.html#player-compatibility), such as VLC or MPV.

## Build
#### Simple executable jar
Simply download sbt
[here](https://www.scala-sbt.org/download), clone this repo and build the app :

```sh
git clone https://gitlab.com/Ignamarte/tress
cd tress
sbt assembly
```

`Tress.jar` is located inside `./target`.

#### Windows executable file

To create a nice and handy `.exe` file, build the jar file as described 
previously, then download jsmooth [here](http://jsmooth.sourceforge.net/), 
add jsmooth installation folder
 your path and and run the script:
 
 ```batch
:: WINDOWS ONLY

set PATH=%PATH%;<YOUR_JSMOOTH_INSTALLATION_FOLDER_WITHOUT_CHEVRONS>
cd scripts
build
```

The .exe file (`Tress.exe`) is located inside `./scripts`.
Be aware that the exe still requires Java 11 or higher.