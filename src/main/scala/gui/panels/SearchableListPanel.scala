package gui.panels

import api.types.collection.APICollection
import gui.stages.MainWindow
import gui.widgets.jfx2sfx.FAGlyphIcon
import org.controlsfx.glyphfont.FontAwesome
import scalafx.application.Platform
import scalafx.scene.Node
import scalafx.scene.control.{Button, ProgressIndicator, TextInputDialog}
import scalafx.scene.image.ImageView

/**
  * Abstract class to enable searches on a list panel
  *
  * @param title         The title of the Panel.
  * @param usePagination Whether the list is displayed using pagination or not.
  *                      If set to true, next objects are displayed when
  *                      pressing the "loadMore" button.
  * @param previous      The previous panel from which the user came from. When
  *                      provided, a "goBack" button is added to the panel.
  * @tparam A            An `APIObject`.
  */
abstract class SearchableListPanel[A](
                              title: String,
                              usePagination: Boolean,
                              previous: Option[Node] = None,
                            )
  extends ListPanel[A](title, usePagination, previous) {

  /**
    * Method to retrieve the message to display in the search dialog.
    *
    * @return The message to display
    */
  def searchDialogMsg: String

  /**
    * Method to retrieve the search results
    * @param query A query string
    * @return An `APICollection` object
    */
  def getSearchResult(query: String): APICollection[A]

  /**
    * Retrieve and display the search results
    * @param query A query string
    */
  private def getAndDisplaySearchResult(query: String): Unit = {
    center = new ProgressIndicator()
    new Thread() {
      override def run(): Unit = {
        val objects = getSearchResult(query)
        Platform.runLater(displayObjects(objects, hideLoadMore = true))
        // Disable button as it can be triggered by scrolling down
        loadMoreBtn.onAction = _ => ()
      }
    }.start()
  }

  // The button to display the search dialog
  val searchButton: Button = new Button("Search") {
    graphic = FAGlyphIcon(FontAwesome.Glyph.SEARCH.getChar, size = 14)
    styleClass.add("grey")
    // The search dialog
    val dialog: TextInputDialog = new TextInputDialog() {
      graphic = new ImageView("images/search.png")
      initOwner(MainWindow)
      title = "Search"
      headerText = searchDialogMsg
      contentText = "Your search query: "
    }

    onMouseClicked = _ => {
      dialog.showAndWait() match {
        case Some(query) => if (query != "") getAndDisplaySearchResult(query)
        case _ =>
      }
    }
  }

  // Add the additional button to the topmenu
  override def initTopMenu(): Unit = {
    super.initTopMenu()
    topMenu.add(searchButton, 3, 0)
  }

}
