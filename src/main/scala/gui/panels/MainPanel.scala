package gui.panels

import gui.widgets.transitions.FadeIn
import scalafx.scene.Node
import scalafx.scene.layout.AnchorPane

object MainPanel extends AnchorPane {

  /* Storage for the current Node.
  This value is used when a panel with a go back capability needs the current
  Node being displayed. This is the case when clicking on the name of a game
  being displayed in a stream popover.
  This is required because the implicit conversions between sfx and jfx
  lose a lot of information. That is required to update the SideMenu for
  instance */
  private var currentNode: Option[Node] = None

  id = "main-panel"

  private def playTransition(): Unit = {
    val transition = new FadeIn(400, this)
    transition.play()
  }

  def displayNode(node: Node): Unit = {
    currentNode = Some(node)
    children = node
    node.styleClass.add("panel")
    AnchorPane.setLeftAnchor(node, 0)
    AnchorPane.setRightAnchor(node, 0)
    AnchorPane.setTopAnchor(node, 0)
    AnchorPane.setBottomAnchor(node, 0)
    playTransition()
    /* Update the side menu */
    node match {
      case _: FollowedPanel => SideMenu.updateActive("followed")
      case _: FollowsPanel => SideMenu.updateActive("live")
      case _: GamesPanel => SideMenu.updateActive("games")
      case _: SettingsPanel => SideMenu.updateActive("settings")
      case _: StreamsPanel => SideMenu.updateActive("streams")
      case _ => SideMenu.updateActive("")
    }
    System.gc()
  }

  def displayGames(): Unit = {
    displayNode(new GamesPanel("Top games"))
    SideMenu.updateActive("games")
  }

  def displayStreams(gameID: Option[String], gameName: String, prev: Node): Unit
  = {
    displayNode(new StreamsPanel(gameName, gameID, Some(prev)))
    SideMenu.updateActive("streams")
  }

  def displayStreams(
                      gameID: Option[String],
                      gameName: String,
                      useCurrentNodeAsPrevious: Boolean = true
                    ): Unit = {
    displayNode(new StreamsPanel(gameName, gameID, currentNode))
  }

  def displayStreams(): Unit = {
    displayNode(new StreamsPanel("Top streams"))
  }

  def displayFollowsPanel(): Unit = {
    displayNode(new FollowsPanel("Followed streams"))
  }

  def displayFollowedPanel(): Unit = {
    displayNode(new FollowedPanel("Followed users"))
  }

  def displaySettings(): Unit = {
    displayNode(new SettingsPanel)
  }

  def displayErrorPanel(): Unit = {
    displayNode(new ErrorPanel)
  }

  def displayLoginPanel(callback: () => Unit): Unit = {
    displayNode(new LoginPanel(callback))
  }

}
