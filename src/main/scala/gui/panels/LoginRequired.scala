package gui.panels

import api.types.User
import errors.NotLoggedInError
import scalafx.application.Platform
import scalafx.scene.layout.Pane

trait LoginRequired extends Pane {

  def initialize(): Unit

  new Thread {
    override def run(): Unit = {
      try {
        val user = User.authenticatedUser
        Platform.runLater(initialize())
      }
      catch {
        case _: NotLoggedInError => Platform.runLater(
          MainPanel.displayLoginPanel(MainPanel.displayFollowsPanel)
        )
      }
    }
  }.start()

}
