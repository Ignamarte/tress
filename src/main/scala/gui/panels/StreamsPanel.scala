package gui.panels

import api.types.collection.{Channels, Streams}
import api.types.{Pagination, Stream}
import gui.stages.MainWindow
import gui.widgets.cards.StreamCard
import scalafx.application.Platform
import scalafx.scene.Node

class StreamsPanel(
                    title: String,
                    gameID: Option[String] = None,
                    previous: Option[Node] = None
                  )
  extends SearchableListPanel[Stream](title, true, previous=previous) {

  def searchDialogMsg: String = "Search a specific stream."

  def createElement(obj: Stream): StreamCard = {
    new StreamCard(obj, MainWindow, false)
  }

  def getSearchResult(query: String): Streams = {
    val channels = Channels.search(query)
    val channelIds = channels.objects.map(c => c.id)
    Streams.getStreams(channelIds).withUser()
  }

  def getNextObjects(pageCursor: String): Streams = {
    Streams.topStreams(
      page = Some(Pagination("after", pageCursor)),
      gameID = gameID
    ).withUser()
  }

  override def initialize(): Unit = {
    super.initialize()
    new Thread {
      override def run(): Unit = {
        val streams = Streams.topStreams(gameID = gameID).withUser()
        Platform.runLater(displayObjects(streams))
      }
    }.start()
  }
}
