package gui.panels

import gui.widgets.control.ClickableButton
import scalafx.geometry.Pos.Center
import scalafx.scene.control.Label
import scalafx.scene.layout.VBox
import util.LoginManager

class LoginPanel(callback: () => Unit) extends VBox {

  spacing = 10
  alignment = Center
  alignmentInParent = Center

  def updateSideMenu(): Unit = {
    SideMenu.updateAvatar()
    callback()
  }

  private val button = new ClickableButton(
      label ="Login",
      () => LoginManager.startOAuth(updateSideMenu)
    )

  children = List(
    new Label("Please login first :"){id="login-label"},
    button
  )
}
