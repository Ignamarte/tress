package gui.panels

import api.types.collection.{Follows, Streams}
import api.types.{Pagination, Stream}
import com.typesafe.scalalogging.LazyLogging
import gui.stages.MainWindow
import gui.widgets.cards.StreamCard
import scalafx.application.Platform
import util.config.UserConfig

class FollowsPanel(title:String)
  extends ListPanel[Stream](title, false) with LazyLogging{

  def createElement(obj: Stream): StreamCard = {
    new StreamCard(obj, MainWindow,true)
  }

  def getNextObjects(pageCursor: String): Streams = {
    val follows = Follows.getSubscriptions()
    follows.getStreams(Some(Pagination("after", pageCursor)))
  }

  override def initialize(): Unit = {
    super.initialize()

    new Thread {
      override def run(): Unit = {
        val follows = Follows.getSubscriptions().getStreams().withUser()
        Platform.runLater(displayObjects(follows))
      }
    }.start()
  }

  // Thread refreshing the list of followed streams every `timeout` minutes
  private val thread = new Thread {
    override def run(): Unit = {
      logger.debug("Starting refresh thread for follows panel")

      val timeout = 300000
      Thread.sleep(timeout)

      while (parent.isNotNull.get()) {
        logger.info("Refreshing follow Panel")

        Platform.runLater(refresh())
        Thread.sleep(timeout)

        logger.debug("Checking if follows panel is still visible.")
      }
      logger.debug("Shutting down refresh thread for follows panel")
    }
  }
  thread.setDaemon(true)

  if (UserConfig.getRefreshFollows) thread.start()

}
