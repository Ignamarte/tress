package gui.panels

import api.types.Stream
import api.types.collection.{Streams, Users}
import com.typesafe.scalalogging.LazyLogging
import errors.StreamLinkMissingError
import gui.stages.StreamWindow
import gui.widgets.alert.ErrorAlert
import gui.widgets.cards.UserCard
import gui.widgets.chat.Chat
import gui.widgets.control.ClickableButton
import gui.widgets.jfx2sfx.FAGlyphIcon
import javafx.scene.input.MouseButton
import org.controlsfx.glyphfont.FontAwesome
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.geometry.Pos.CenterLeft
import scalafx.scene.control.{Button, Label, ProgressIndicator}
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.{HBox, VBox}
import util.config.UserConfig
import util.{DateUtil, StreamLink}

class StreamDetailPanel(stream: Stream, notificationWindow: StreamWindow)
  extends VBox with LazyLogging {

  styleClass.add("white-bg")

  private val INFO_PANEL_HEIGHT = 100

  /***********************************/
  /*               INFO              */
  /***********************************/
  private val streamInfoContainer = new HBox(){
    styleClass.add("padding-1em")
    alignment = CenterLeft
    spacing = 10
  }

  /*
  * Will be replaced by a new UserCard once the associated user is retrieved
  * in a child thread.
  */
  private val logoLoading = new ProgressIndicator()

  private val streamTitle = new Label(stream.title){
    wrapText = true
  }
  private val streamUptime = new Label(DateUtil.uptimeAsString(stream.started_at)){
    graphic = FAGlyphIcon(FontAwesome.Glyph.CLOCK_ALT.getChar, size = 14)
  }
  private val streamViewer = new Label(stream.viewer_count.toString){
    graphic = FAGlyphIcon(FontAwesome.Glyph.USERS.getChar, size = 14)
  }
  private val streamUptimeViewer = new HBox() {
    styleClass.add("purple-text-container")
    spacing = 3
  }

  streamUptimeViewer.children.addAll(
    streamViewer,
    streamUptime
  )

  private val infoContainer = new VBox() {
    maxHeight = INFO_PANEL_HEIGHT
  }

  infoContainer.children.addAll(
    streamTitle,
    streamUptimeViewer
  )

  streamInfoContainer.children.addAll(
    logoLoading,
    infoContainer
  )

  /***********************************/
  /*           TWITCH CHAT           */
  /***********************************/
  private val chat = new Chat(stream)

  /***********************************/
  /*            Controls             */
  /***********************************/

  private val reloadChatBtn: Button = new ClickableButton("", () => reloadChat()) {
    prefWidth <== height
    graphic = FAGlyphIcon(FontAwesome.Glyph.REFRESH.getChar, size = 14)
  }

  private val clearChatBtn: Button = new ClickableButton("", () => chat.clear()){
    prefWidth <== height
    graphic = FAGlyphIcon(FontAwesome.Glyph.ERASER.getChar, size = 14)
  }

  private val launchStreamBtn:Button = new ClickableButton("", () => launchStream()){
    prefWidth <== height
    graphic = FAGlyphIcon(FontAwesome.Glyph.PLAY_CIRCLE_ALT.getChar, size = 14)
  }

  private val toBottomBtn:Button = new ClickableButton("",() => chat.scrollDown()){
    prefWidth <== height
    graphic = FAGlyphIcon(FontAwesome.Glyph.ARROW_CIRCLE_DOWN.getChar, size = 14)
  }

  private val toTopBtn:Button = new ClickableButton("",() => chat.scrollUp()){
    prefWidth <== height
    graphic = FAGlyphIcon(FontAwesome.Glyph.ARROW_CIRCLE_UP.getChar, size = 14)
  }

  private val toggleGifsBtn: Button = new ClickableButton("", () => ()) {
    private def updateGraphic(): Unit = {
      if(UserConfig.getDisableGifEmotes){
        graphic = FAGlyphIcon(FontAwesome.Glyph.TOGGLE_OFF.getChar, size = 14)
      } else {
        graphic = FAGlyphIcon(FontAwesome.Glyph.TOGGLE_ON.getChar, size = 14)
      }
    }
    updateGraphic()
    text = "GIFs"
    onMouseClicked = event => {
      if (event.getButton == MouseButton.PRIMARY) {
        UserConfig.setDisableGifEmotes(!UserConfig.getDisableGifEmotes)
        updateGraphic()
      }
    }
  }

  private val controlsContainer: HBox = new HBox() {
    spacing = 4d
    styleClass.add("padding-1em-no-bottom")
    alignment = CenterLeft
  }

  controlsContainer.children.addAll(
    toTopBtn,
    toBottomBtn,
    reloadChatBtn,
    clearChatBtn,
    launchStreamBtn,
    toggleGifsBtn
  )

  /* Display the stream details */
  children.addAll(
    controlsContainer,
    streamInfoContainer,
    chat
  )

  private def launchStream() = {
    notificationWindow.displayNotification(message=f"Stream started")
    try {
      val message = "It looks like streamlink failed to load your stream."
      StreamLink.launch(
        stream.user_login.get,
        detail => new ErrorAlert(message, detail) // Display error on failure
      )
    } catch {
      case _:StreamLinkMissingError => new ErrorAlert(
        "StreamLink is missing, please check your configuration.",
        "StreamLink binary is missing from the configuration file."
      )
    }
  }

  /**
   * Reload the chat
   * Disable the reload button to avoid queuing multiple reloads
   * The button should always be re-enabled in case of failure
   * The underlying call to a Future ensures that the callback is always ran
   */
  private def reloadChat(): Unit = {
    reloadChatBtn.disable = true
    chat.reload(callback = () => reloadChatBtn.disable = false)
  }

  /* Shutdown the stream and the connection to the chat when called */
  def shutdown(): Unit = {
    refreshThread.interrupt()
    chat.shutdown()
  }

  /* Get the user object and display its profile picture */
  new Thread {
    override def run(): Unit = {
      val user = Users.getUsers(List(stream.user_id)).objects.head
      Platform.runLater({
        val userCard = new UserCard(user, notificationWindow, followed = false)
        userCard.maxWidth = INFO_PANEL_HEIGHT
        userCard.minWidth = INFO_PANEL_HEIGHT
        streamInfoContainer.children.remove(logoLoading)
        streamInfoContainer.children.add(0, userCard)
      })
    }
  }.start()

  // Thread refreshing the stream info every `timeout` minutes
  private val refreshThread = new Thread {
    override def run(): Unit = {
      logger.debug("Starting refresh thread for stream details panel")

      val timeout = 60000

      try {
        Thread.sleep(timeout)
      } catch {
        case _: InterruptedException => logger.debug("Shutdown requested")
      }

      while (notificationWindow.showing.get) {
        logger.info("Refreshing stream details Panel")

        val newStreamOption = Streams.getStream(stream.user_id)
        if (newStreamOption.isDefined){
          val newStream = newStreamOption.get
          Platform.runLater({
            streamViewer.text = newStream.viewer_count.toString
            streamUptime.text = DateUtil.uptimeAsString(stream.started_at)
            streamTitle.text = newStream.title
          })
        } else {
          Platform.runLater({
            streamViewer.text = "0"
            streamUptime.text = "offline"
          })
        }

        try {
          Thread.sleep(timeout)
        } catch {
          case _: InterruptedException => logger.debug("Shutdown requested")
        }

        logger.debug("Checking if stream details panel is still visible.")
      }
      logger.debug("Shutting down refresh thread for stream details panel")
    }
  }
  refreshThread.setDaemon(true)
  refreshThread.start()
}
