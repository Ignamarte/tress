package gui.panels

import gui.widgets.control.{ChoiceSelector, FileDialogButton, SimpleCheckBox, Title}
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.scene.control.Label
import scalafx.scene.layout.GridPane
import util.StreamLink
import util.config.UserConfig

class SettingsPanel extends GridPane {

  initialize()

  padding = Insets(15,15,15,15)
  hgap = 10
  vgap = 5

  def initialize(): Unit = {
    children.clear()
    add(new Title("Settings"), 0,0)

    // Streamlink
    add(new Label("Streamlink :"), 0, 1)
    add(
      new FileDialogButton(
        dialogTitle = "Select the streamlink executable",
        UserConfig.getStreamLink,
        UserConfig.setStreamLink
      ),
      1, 1
    )

    // VideoPlayer
    add(new Label("Player :"), 0, 2)
    add(
      new FileDialogButton(
        dialogTitle = "Select a video player executable",
        UserConfig.getPlayer,
        UserConfig.setPlayer
      ),
      1, 2
    )

    // Quality
    add(new Label("Stream quality :"), 0, 3)
    add(
      new ChoiceSelector(
        ObservableBuffer(StreamLink.getStreamQualities),
        UserConfig.getQuality,
        UserConfig.setQuality),
      1,3)
  }

  // Autorefresh Follows
  add(new Label("Auto refresh followed streams :"), 0, 4)
  add(
    new SimpleCheckBox(
      UserConfig.getRefreshFollows,
      UserConfig.setRefreshFollows),
    1,4)

  // Allow gifs in chat
  add(new Label("Disable GIF emotes :"), 0, 5)
  add(
    new SimpleCheckBox(
      UserConfig.getDisableGifEmotes,
      UserConfig.setDisableGifEmotes),
    1,5)

  // Open chat with streams
  add(new Label("Open chat when launching a stream :"), 0, 6)
  add(
    new SimpleCheckBox(
      UserConfig.getOpenChatWithStream,
      UserConfig.setOpenChatWithStream),
    1,6)
}
