package gui.panels

import gui.widgets.control.Title
import scalafx.geometry.Pos.Center
import scalafx.scene.layout.VBox

class ErrorPanel extends VBox{

  private val text = new Title("Something went wrong ...")

  alignment = Center
  children = text

}
