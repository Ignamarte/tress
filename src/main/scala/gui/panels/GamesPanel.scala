package gui.panels

import api.types.collection.Games
import api.types.{Game, Pagination}
import gui.widgets.cards.GameCard
import scalafx.application.Platform

class GamesPanel (title: String)
  extends SearchableListPanel[Game](title, true) {

  def searchDialogMsg: String = "Search a specifc game.\n" +
    "Search results match with the beginning of the game's name."

  def getSearchResult(query: String): Games = {
    Games.search(query)
  }

  def getNextObjects(pageCursor: String): Games = {
    Games.topGames(page = Some(Pagination("after", pageCursor)))
  }

  def createElement(obj: Game): GameCard = {
    new GameCard(obj, this)
  }

  override def initialize(): Unit = {
    super.initialize()

    new Thread {
      override  def run(): Unit = {
        val games = Games.topGames()
        Platform.runLater(displayObjects(games))
      }
    }.start()
  }
}
