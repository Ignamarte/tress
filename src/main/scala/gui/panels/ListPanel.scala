package gui.panels

import api.types.collection.APICollection
import gui.widgets.control.Title
import gui.widgets.jfx2sfx.FAGlyphIcon
import gui.widgets.transitions.FadeIn
import org.controlsfx.glyphfont.FontAwesome
import scalafx.application.Platform
import scalafx.geometry.Pos.TopCenter
import scalafx.geometry.{Insets, _}
import scalafx.scene.Node
import scalafx.scene.control.{Button, Label, ProgressIndicator, ScrollPane}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.{BorderPane, ColumnConstraints, FlowPane, GridPane}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


/** An abstract class to display a list of objects retrieved from the twitch API.
  *
  * @param title The title of the Panel.
  * @param usePagination Whether the list is displayed using pagination or not.
  *                      If set to true, next objects are displayed when
  *                      pressing the "loadMore" button.
  * @param previous The previous panel from which the user came from. When
  *                 provided, a "goBack" button is added to the panel.
* */
abstract class ListPanel[A](
                             title: String,
                             usePagination: Boolean,
                             previous: Option[Node] = None,
                           )
  extends BorderPane with LoginRequired {
  minWidth = 100

  private val TOP_MENU_COLS = 7

  center = new ProgressIndicator

  private val grid = new FlowPane() {
    styleClass.add("white-bg")
    padding = Insets(15,15,15,15)
    alignment = TopCenter
    hgap = 10
    vgap = 20
  }

  protected val topMenu: GridPane = new GridPane() {
    hgrow = Always
    padding = Insets(15,0,15,0)
    alignment = TopCenter
    hgap = 10
    vgap = 20
  }

  // A loading animation
  protected val loadMoreProgress: ProgressIndicator = new ProgressIndicator() {
    styleClass.add("grey-accent")
    prefHeight = 15
    new FadeIn(300, this).play()
  }

  // The button to load more items
  protected val loadMoreBtn: Button = new Button() {
    styleClass.add("grey")
    minWidth <==
      grid.width - (grid.padding.value.getLeft + grid.padding.value.getRight)
    maxWidth <== minWidth
  }

  private val gridContainer = new ScrollPane() {
    id = "list-grid"
    fitToWidth = true
    fitToHeight = true
    vbarPolicy = ScrollPane.ScrollBarPolicy.Always
    content = grid

    // Update when reaching bottom
    vvalue.onChange(if (vvalue.value == 1.0d) loadMoreBtn.fire())
  }

  // Constraint for the topmenu
  private val constraint = new ColumnConstraints()
  constraint.percentWidth = 100/TOP_MENU_COLS
  constraint.halignment = HPos.Center

  // Mandatory methods
  def getNextObjects(pageCursor: String): APICollection[A]
  def createElement(obj: A): Node

  def refresh(): Unit = initialize()
  def initialize(): Unit = {
    children.clear()
    center = new ProgressIndicator
  }

  protected def addObjects(objects: List[A]): Unit = {
    val e = createElement(objects.head)
    grid.children.add(e)

    if (objects.isDefinedAt(1)) {
      addObjects(objects.tail)
    }
  }

  protected def initGrids(): Unit = {
    top = topMenu
    center = gridContainer
    grid.children.clear()
  }

  protected def initTopMenu(): Unit = {

    topMenu.children.clear()
    topMenu.columnConstraints = List.fill(TOP_MENU_COLS)(constraint)

    // Go back button
    if (previous.isDefined) {
      val goBackBtn = new Button("Back") {
        graphic =
          FAGlyphIcon(FontAwesome.Glyph.ARROW_CIRCLE_LEFT.getChar, size = 14)
        styleClass.add("grey")
        onMouseClicked = _ => MainPanel.displayNode(previous.get)
      }
      topMenu.add(goBackBtn, 2, 0)
    }

    // Refresh button
    val refreshBtn = new Button("Refresh") {
      graphic = FAGlyphIcon(FontAwesome.Glyph.REFRESH.getChar, size = 14)
      styleClass.add("grey")
      onMouseClicked = _ => refresh()
    }

    // Scroll to Top / To Bottom Buttons

    val toTopBtn = new Button("Top") {
      graphic =
        FAGlyphIcon(FontAwesome.Glyph.ARROW_CIRCLE_O_UP.getChar, size = 14)
      styleClass.add("grey")
      onMouseClicked = _ => gridContainer.setVvalue(0.0d)
    }

    val toBottomBtn = new Button("Bottom") {
      graphic =
        FAGlyphIcon(FontAwesome.Glyph.ARROW_CIRCLE_O_DOWN.getChar, size = 14)
      styleClass.add("grey")
      onMouseClicked = _ => gridContainer.setVvalue(1.0d)
    }

    // Add the buttons to the menu
    topMenu.add(toTopBtn, 4, 0)
    topMenu.add(toBottomBtn, 5, 0)
    topMenu.add(refreshBtn, 6, 0)
    topMenu.add(new Title(title), 0, 0, 2, 1)
  }

  protected def displayObjects(
                      objects: APICollection[A],
                      isUpdate: Boolean = false,
                      hideLoadMore: Boolean = false
                    ): Unit = {

    // The is a freshly create panel
    if(!isUpdate) {
      initGrids()
      initTopMenu()
    }

    // There are some things to display
    if(objects.objects.isDefinedAt(0)){
      addObjects(objects.objects)
      // Display the "load more" btn if pagination is required and not
      // explicitly disabled with hideLoadMore = true
      if(usePagination && !hideLoadMore) {
        displayLoadMoreBtn(objects.cursor)
      }
    // There is nothing to display
    } else if (!isUpdate) {
      grid.children.add(Label("Sorry, we found nothing to display here ..."))
    }
  }

  // Display the loadMore button and set its method when pressed
  protected def displayLoadMoreBtn(currentCursor: String): Unit = {
    // Add the button to the panel
    grid.children.add(loadMoreBtn)
    loadMoreBtn.graphic = null
    loadMoreBtn.text = "Load more"
    // When clicking the button, display loading animation and start
    // retrieving the next objects
    loadMoreBtn.onAction = _ => {
      // Remove the button and add the loading animation
      loadMoreBtn.setGraphic(loadMoreProgress)
      loadMoreBtn.text = "Loading"
      displayNextObjects(currentCursor)
    }
  }

  /**
   * Display the objects in an async thread
   * @param cursor The current cursor to retrive the next batch of items
   */
  def displayNextObjects(cursor: String): Unit = Future[Unit]{
    val nextObjects = try getNextObjects(cursor) catch {
      // If retrieving the items fails remove the loading animation,
      // display the button again with an error message
      case e:Throwable => Platform.runLater({
        loadMoreBtn.text = s"Error, try again (${e.getMessage})"
        loadMoreBtn.graphic = null
      })
      throw e
    }
    //Display the items and remove the loading animation
    Platform.runLater({
      grid.children.remove(grid.children.get(grid.children.size()-1))
      displayObjects(nextObjects, isUpdate = true)
    })
  }
}
