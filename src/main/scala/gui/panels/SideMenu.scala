package gui.panels

import api.types.User
import errors.NotLoggedInError
import gui.widgets.UserAvatar
import gui.widgets.control.MenuButton
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.geometry.Pos.Center
import scalafx.scene.image.ImageView
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.{AnchorPane, Region, VBox}

object SideMenu extends VBox{

  id = "side-menu"
  padding = Insets(20,0,20,0)
  spacing = 5
  minWidth = 100

  private val userImg = new UserAvatar{radius <== width / 6}

  def updateAvatar(): Unit = {
    val thread = new Thread {
      override def run(): Unit = {
        try {
          val user = User.authenticatedUser
          userImg.update(user.profile_image_url, user.display_name)
          Platform.runLater(children.add(userImg))
        } catch {
          case _: NotLoggedInError =>
            Platform.runLater(children.remove(userImg))
          case _: Throwable =>
            Thread.sleep(3000)
            updateAvatar()
        }
      }
    }
    thread.setDaemon(true)
    thread.start()
  }

  private val logo = new AnchorPane(){
    id = "twitch-logo"
    minWidth = 100
    maxWidth = 150
    alignment = Center
    children = new ImageView("images/logo.png"){
      preserveRatio = true
      fitWidth <== width
    }
  }

  val btnMap: Map[String, MenuButton] = Map(
    "games" -> new MenuButton("Games", width, MainPanel.displayGames),
    "streams" -> new MenuButton("Streams", width, MainPanel.displayStreams),
    "live" -> new MenuButton("Live", width, MainPanel.displayFollowsPanel),
    "followed" -> new MenuButton("Followed", width, MainPanel.displayFollowedPanel),
    "settings" -> new MenuButton("Settings", width, MainPanel.displaySettings),
  )

  /* Update the currently active button */
  def updateActive(btnKey: String): Unit = {
    btnMap.values.foreach(btn => btn.styleClass.remove("active"))
    btnMap.get(btnKey) match {
      case Some(btn) => btn.styleClass.add("active")
      case None =>
    }
  }

  children_=(List(
    logo,
    btnMap("games"),
    btnMap("streams"),
    btnMap("live"),
    btnMap("followed"),
    btnMap("settings"),
    new Region(){vgrow = Always}
  ))

  Platform.runLater(updateAvatar())

}
