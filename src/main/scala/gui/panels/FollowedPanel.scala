package gui.panels

import api.types.collection.{Follows, Users}
import api.types.{Pagination, User}
import gui.stages.MainWindow
import gui.widgets.cards.UserCard
import scalafx.application.Platform


class FollowedPanel(title:String)
  extends ListPanel[User](title, false) {

  def createElement(obj: User): UserCard = {
    new UserCard(obj, MainWindow)
  }

  def getNextObjects(pageCursor: String): Users = {
    val follows = Follows.getSubscriptions()
    follows.getUsers(Some(Pagination("after", pageCursor)))
  }

  override def initialize(): Unit = {
    super.initialize()

    new Thread {
      override  def run(): Unit = {
        val follows = Follows.getSubscriptions().getUsers()
        Platform.runLater(displayObjects(follows))
      }
    }.start()
  }
}
