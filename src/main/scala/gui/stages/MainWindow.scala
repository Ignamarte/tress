package gui.stages

import api.webserver.WebServer
import errors.APIError
import gui.panels.{MainPanel, SideMenu}
import gui.widgets.alert.ErrorAlert
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.Platform
import scalafx.scene.Scene
import scalafx.scene.image.Image
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.{ColumnConstraints, GridPane, RowConstraints}
import scalafx.scene.text.Font
import util.config.UserConfig
import scalafx.Includes._
import scalafx.stage.Screen

object MainWindow extends PrimaryStage with NotificationStage {

  val DEFAULT_HEIGHT = 550d
  val DEFAULT_WIDTH = 800d

  title.value = "Tress"

  height = UserConfig.getHeight.getOrElse(DEFAULT_HEIGHT)
  width = UserConfig.getWidth.getOrElse(DEFAULT_WIDTH)

  maximized = UserConfig.getIsMaximized.getOrElse(false)

  minHeight = DEFAULT_HEIGHT
  minWidth = DEFAULT_WIDTH

  icons += new Image("/images/icon.png")

  /* Set the panel to display the notifications on */
  notificationPane.setContent(MainPanel)

  /* *** Main Scene *** */

  scene = new Scene {

    stylesheets += getClass.getResource("/css/window.css").toExternalForm
    stylesheets += getClass.getResource("/css/scrollbar.css").toExternalForm
    Font.loadFont(getClass.getResource("/font/custom.ttf").toExternalForm, 10)

    root = new GridPane() {

      val constraintMenu = new ColumnConstraints()
      val constraintPanel = new ColumnConstraints()
      constraintMenu.setPrefWidth(Screen.primary.bounds.maxX * 0.10)
      constraintMenu.setMinWidth(200)
      constraintPanel.setHgrow(Always)
      columnConstraints = List(constraintMenu, constraintPanel)

      val rowConstraint = new RowConstraints()
      rowConstraint.setVgrow(Always)
      rowConstraints = List.fill(1)(rowConstraint)

      add(SideMenu, 0, 0)
      add(notificationPane, 1, 0)
    }
  }

  // Display the default panel after loading the scene

  Platform.runLater(MainPanel.displayFollowsPanel())

  // Register size changes

  width.onChange { (source, oldValue, newValue) =>
    if (!maximized()) UserConfig.setWidth(newValue.doubleValue())
  }

  height.onChange { (source, oldValue, newValue) =>
    if (!maximized()) UserConfig.setHeight(newValue.doubleValue())
  }

  maximized.onChange {(source, oldValue, newValue) =>
    UserConfig.setMaximized(maximized())
  }

  // Error handlers

  private def ignoreError(thread: Thread, error: Throwable): Unit = {}

  private def showError(thread: Thread, error: Throwable): Unit = {
    error match {
      case APIError(status, msg) => Platform.runLater({
        new ErrorAlert(
          f"The API returned an unexpected status code: $status",
          msg
        )
      })
      case e:Throwable => Platform.runLater({
        new ErrorAlert(
          f"An unexpected error occurred : \n $e",
          e.getStackTrace.mkString("\n")
        )
      })
    }
  }

  Thread.setDefaultUncaughtExceptionHandler(showError)

  // Actions to do when closing the gui

  onCloseRequest = _ =>
  {
    Thread.setDefaultUncaughtExceptionHandler(ignoreError)
    WebServer.stop()
  }

}
