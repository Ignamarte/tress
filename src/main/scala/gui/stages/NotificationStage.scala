package gui.stages

import gui.panels.MainPanel
import javafx.stage.Stage
import org.controlsfx.control.NotificationPane
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.Platform
import scalafx.scene.Node
import scalafx.scene.layout.Pane

trait NotificationStage {

  val notificationPane = new NotificationPane

  notificationPane.setCloseButtonVisible(false)

  def displayNotification(message: String, graphic: Node = new Pane()): Unit = {
    new Thread {
      override def run(): Unit = {
        // While this is probably not perfect,
        // this allows us to queue notifications
        while (notificationPane.isShowing) {
          Thread.sleep(200)
        }

        // Make sure the notification has some time to hide since
        // isShowing is false when the notification *starts* hidding
        Thread.sleep(200)

        Platform.runLater({
          notificationPane.setGraphic(graphic)
          notificationPane.setText(message)
          notificationPane.show()
        })

        notificationPane.setOnShown(_ => {
          new Thread {
            override def run(): Unit = {
              Thread.sleep(2500)
              notificationPane.hide()
            }
          }.start()
        })
      }
    }.start()
  }
}
