package gui.stages

import api.types.Stream
import gui.panels.StreamDetailPanel
import gui.stages.MainWindow.{DEFAULT_HEIGHT, DEFAULT_WIDTH, minHeight, minWidth}
import scalafx.scene.Scene
import scalafx.scene.image.Image
import scalafx.scene.layout.{AnchorPane, HBox}
import scalafx.scene.text.Font
import scalafx.stage.Stage

class StreamWindow(stream: Stream) extends Stage with NotificationStage {
  val DEFAULT_HEIGHT = 800d
  val DEFAULT_WIDTH = 500d

  height = DEFAULT_HEIGHT
  width = DEFAULT_WIDTH

  minWidth = 300d

  icons.add(new Image("/images/icon.png"))

  title = stream.user_name

  private val streamDetails = new StreamDetailPanel(stream, this)
  notificationPane.setContent(streamDetails)
  private val container = new HBox{children.add(notificationPane)}
  notificationPane.prefWidthProperty().bind(container.width)

  scene = new Scene() {
    stylesheets.addAll(
      getClass.getResource("/css/chat.css").toExternalForm,
      getClass.getResource("/css/window.css").toExternalForm,
      getClass.getResource("/css/scrollbar.css").toExternalForm
    )
    Font.loadFont(getClass.getResource("/font/chat.ttf").toExternalForm, 10)
    Font.loadFont(getClass.getResource("/font/custom.ttf").toExternalForm, 10)

    root = new AnchorPane {
      children = container
      AnchorPane.setLeftAnchor(container, 0)
      AnchorPane.setRightAnchor(container, 0)
      AnchorPane.setTopAnchor(container, 0)
      AnchorPane.setBottomAnchor(container, 0)
    }

    onCloseRequest = _ => streamDetails.shutdown()
  }
}
