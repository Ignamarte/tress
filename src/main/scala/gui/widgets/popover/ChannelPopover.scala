package gui.widgets.popover

import api.types.{Follow, User}
import gui.panels.MainPanel
import gui.stages.{MainWindow, NotificationStage}
import org.controlsfx.control.PopOver
import scalafx.application.Platform
import scalafx.geometry.Insets
import scalafx.geometry.Pos.Center
import scalafx.scene.Node
import scalafx.scene.control.Button
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.VBox
import util.Browser

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


/** Abstract class for building context menu popping up when right clicking
 * on a gui element related to a channel (a stream or a user for instance)
 *
 * This class provides elements common to any channel popover menu but it's up
 * to the implementation to choose what to display by constructing the
 * childrenList
 *
 * If followed is false, we will send a request to the API to check if the
 * current user follows the channel or not. In fact false means that we
 * don't know.
 *
 * @constructor Created a context menu for a stream GUI element
 * @param followed Whether the user is known to be currently following the
 *                 streamer or not
 * @param image A thumbnail used when displaying a notification
 * @param userLogin The login of the user
 * @param userID The ID of the user
 * @param userName The name of the user
 * @param notificationWindow An instance of [[gui.stages.NotificationStage]] to
 *                           display the notifications
 */
abstract class ChannelPopover(
                               followed: Boolean,
                               image: Image,
                               userLogin: String,
                               userID: String,
                               userName: String,
                               notificationWindow: NotificationStage
                             ) extends PopOver {

  val childrenList: List[Node]

  def display(): Unit

  // Container for the menu's content
  protected val content: VBox = new VBox(){
    minWidth = 150
    margin = Insets(10)
    spacing = 7
    vgrow = Always
    alignment = Center
  }

  // Configuration of the popover
  setArrowLocation(PopOver.ArrowLocation.TOP_CENTER)
  setContentNode(content)
  setDetachable(false)

  // Open the channel's page in a browser
  protected val channelBtn: Button = new Button {
    text = "Channel Page"
    prefWidth <== content.width
    onAction = _ => {
      val url = f"https://www.twitch.tv/$userLogin"
      Browser.openURL(url)
    }
  }

  // Open the popout chat in a browser
  protected val webChatBtn: Button = new Button {
    text = "Web Chat"
    prefWidth <== content.width

    onAction = _ => {
      val url = f"https://www.twitch.tv/popout/$userLogin/chat"
      Browser.openURL(url)
    }
  }

  def displayNotification(msg: String): Unit = {
    notificationWindow.displayNotification(
      msg,
      new ImageView(image){
        fitWidth = 50
        preserveRatio = true
      }
    )
  }

  protected def followChannel(): Unit = {
    User.authenticatedUser.follow(userID).onComplete {
      case Success(value) => Platform.runLater({
        followBtn.text = "Unfollow"
        followBtn.onAction = _ => unfollowChannel()
        displayNotification(f"You are now following $userName.")
      })
      case Failure(exception) => Platform.runLater({
        println(exception)
        displayNotification(f"Failed to follow $userName !")
      })
    }
  }

  protected def unfollowChannel(): Unit = {
    User.authenticatedUser.unfollow(userID).onComplete {
      case Success(value) => Platform.runLater({
        followBtn.text = "Follow"
        followBtn.onAction = _ => followChannel()
        displayNotification(f"You are not following $userName anymore.")
      })
      case Failure(exception) => Platform.runLater({
        displayNotification(f"Failed to unfollow $userName !")
      })
    }
  }

  // Follow or unfollow the streamer
  protected val followBtn: Button = new Button {

    text = "Checking follow status ..."
    prefWidth <== content.width

    if (! followed) {
      Follow.getSubscription(User.authenticatedUser.id, userID).onComplete {
        case Success(follow) => follow match {
          case Some(e) =>
            Platform.runLater({text = "Unfollow"})
            onAction = _ => unfollowChannel()
          case None =>
            Platform.runLater({text = "Follow"})
            onAction = _ => followChannel()
        }
        case Failure(e) =>
          Platform.runLater({
            text = "Follow (check failed)"})
          onAction = _ => followChannel()
      }
    } else {
      text = "Unfollow"
      onAction = _ => unfollowChannel()
    }
  }



  // When displayed, call the implementation of display()
  setOnShowing(_ => {
    content.children = childrenList
    display()
  })
}

