package gui.widgets.popover

import api.types.User
import gui.stages.NotificationStage
import scalafx.scene.control.Label
import scalafx.scene.image.Image

/**
 * The popover menu when right clicking on a user's channel.
 *
 * Implements `ChannelPopover`.
 *
 * @param user The `User` object associated to the channel
 * @param followed Whether the user is known to be currently following the
 *                 streamer or not
 * @param image A thumbnail used when displaying a notification
 * @param notificationWindow An instance of [[gui.stages.NotificationStage]] to
 *                           display the notifications
 */
class UserPopover (
                    user: User,
                    followed: Boolean,
                    image: Image,
                    notificationWindow: NotificationStage)
  extends ChannelPopover(
    followed,
    image,
    user.login,
    user.id,
    user.display_name,
    notificationWindow
  ) {

  override val childrenList = List(
    new Label(user.display_name){prefWidth <== content.width},
    channelBtn,
    webChatBtn,
    // followBtn // removed as twitch disabled this feature :(
  )

  override def display(): Unit = {}

}
