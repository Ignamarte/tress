package gui.widgets.popover

import api.types.Stream
import gui.panels.MainPanel
import gui.stages.{NotificationStage, StreamWindow}
import javafx.scene.input.{MouseButton, MouseEvent}
import scalafx.application.Platform
import scalafx.scene.control.{Button, Label, ProgressIndicator}
import scalafx.scene.image.Image
import scalafx.Includes._
import scalafx.scene.input.MouseButton.Primary
import util.Browser

/** The context menu popping up when right clicking on a specific stream
 *
 * The game name is loaded with an async task because twitch's API only returns
 * the ID of the game.
 *
 * @constructor Create a context menu for a stream GUI element
 * @param stream The stream object to witch the menu is attached to
 * @param followed Whether the user is known to be currently following the
 *                 streamer or not
 * @param image The thumbnail of the stream used when displaying a notification
 * @param notificationWindow An instance of [[gui.stages.NotificationStage]] to
 *                           display the notifications
 */

class StreamPopover(
                     stream: Stream,
                     followed: Boolean,
                     image: Image,
                     notificationWindow: NotificationStage
                   )
  extends ChannelPopover(
    followed,
    image,
    stream.user_login.get,
    stream.user_id,
    stream.user_name,
    notificationWindow: NotificationStage
  ) {


  // A loading icon displayed while retrieving the game's name
  private val gameNameSpinner = new ProgressIndicator() {
    prefWidth <== content.width / 10d
    prefHeight <== prefWidth
  }

  private val gameNameLabel = new Label("") {
    styleClass.add("clickable")

    onMouseClicked = (event: MouseEvent) => {
      if (event.getButton == MouseButton.PRIMARY) MainPanel.displayStreams(
        Some(stream.game_id),
        stream.getGameName
      )
    }
  }

  private val chatBtn = new Button {
    text = "Chat"
    prefWidth <== content.width

    onAction = _ => new StreamWindow(stream).show()
  }

  // Set the content of the menu
  override val childrenList = List(
    new Label(stream.user_name){prefWidth <== content.width},
    gameNameSpinner,
    channelBtn,
    chatBtn,
    webChatBtn,
    // followBtn // removed as twitch disabled this feature :(
  )

  // When displayed, start the async task retrieving the game name and
  // add content
  override def display(): Unit = {
    new Thread() {
      override def run(): Unit = {
        val msg = try {
          stream.getGameName
        } catch {
          case _: Throwable =>
            "Failed to retrieve \n the current game"
        }
        Platform.runLater({
          content.children.remove(gameNameSpinner)
          gameNameLabel.text = msg
          Platform.runLater(content.children.add(1, gameNameLabel))
        })
      }
    }.start()
  }
}

