package gui.widgets.popover

import org.controlsfx.control.PopOver
import scalafx.geometry.Insets
import scalafx.scene.control.{Button, Label}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.VBox
import util.LoginManager

class LoggedUserPopover extends PopOver(){

  private val content: VBox = new VBox(){
    minWidth = 100
    margin = Insets(10)
    spacing = 10
    vgrow = Always
  }

  setArrowLocation(PopOver.ArrowLocation.BOTTOM_CENTER)
  setContentNode(content)
  setDetachable(false)

  private val nameLabel = new Label() {
    prefWidth <== content.width
  }

  private val logoffBtn = new Button("Logout") {
    prefWidth <== content.width
    onMouseClicked = _ => {
      LoginManager.logoff()
      hide()
    }
  }

  setOnShowing(_ =>
    content.children = List(
      nameLabel,
      logoffBtn
    )
  )

  def updateLabel(username: String): Unit = {
    nameLabel.setText(username)
  }

}