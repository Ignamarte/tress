package gui.widgets.img

import javafx.scene.input.MouseButton
import javafx.stage.PopupWindow
import scalafx.Includes._
import scalafx.scene.CacheHint
import scalafx.scene.control.Tooltip
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.input.{Clipboard, ClipboardContent, MouseEvent}
import scalafx.util.Duration

/**
 * A twitch emote as an ImageView.
 * Images are cached in order to avoid re-downloading them everytime.
 * @param url The URL to the image
 * @param name The name of the emote (chat code or whatever)
 * @param onClick An option function to call when clicking on the emote
 */
class EmoteImg(url: String, urlBig: String, name: String, onClick: () => Unit = () =>())
  extends ImageView() {

  preserveRatio = true
  pickOnBounds = true
  cache = true
  cacheHint = CacheHint.Speed
  styleClass.add("twitch-emote")

  private val img = EmoteImg.getFromCache(url)

  if (img.progress.value == 1d)
    image = img
  else {
    image = new Image("images/emote_placeholder.png")
    img.progress.onChange((progress,_,_) => {
      if (progress.value == 1d) image = img
    })
  }

  fitHeight <== min(img.height, 30)

  /* Display the name and emote when hovering */
  private val emoteTooltip = new Tooltip(name){
    showDelay = Duration(0)
    hideDelay = Duration(100)
    anchorLocation = PopupWindow.AnchorLocation.WINDOW_BOTTOM_LEFT
  }
  Tooltip.install(node = this, emoteTooltip)
  emoteTooltip.onShown = _ => {
    val biggerImg = new ImageView(EmoteImg.getFromCache(urlBig))
    emoteTooltip.graphic = biggerImg
  }

  /* Copy the emote name when clicking on it */
  private val content = new ClipboardContent
  onMouseClicked = (event: MouseEvent) => {
    // event.button throws a strange error so we fallback to javafx
    if (event.getButton == MouseButton.PRIMARY) {
      content.putString(name)
      Clipboard.systemClipboard.content = content
      onClick()
    }
  }
}

object EmoteImg {
  private val emoteCache = collection.mutable.Map.empty[String, Image]

  def getFromCache(url: String): Image = synchronized {
    if (emoteCache.contains(url)) {
      emoteCache(url)
    } else {
      val img = new Image(url, backgroundLoading = true)
      emoteCache.put(url, img)
      img
    }
  }
}
