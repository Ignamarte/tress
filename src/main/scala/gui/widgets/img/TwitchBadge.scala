package gui.widgets.img

import api.types.collection.badges.BadgeImage
import javafx.stage.PopupWindow
import scalafx.Includes.min
import scalafx.scene.control.Tooltip
import scalafx.scene.image.{Image, ImageView}
import scalafx.util.Duration
import scalafx.Includes._

/**
 * A twitch chat badge as an ImageView.
 * Images are cached in order to avoid re-downloading them everytime.
 * @param badge A `BadgeImage` object
 */
class TwitchBadge(badge: BadgeImage) extends ImageView() {
  preserveRatio = true
  pickOnBounds = true
  cache = true

  styleClass.add("twitch-emote")

  private val img = TwitchBadge.getFromCache(badge.image_url_1x)

  image = img

  fitHeight <== min(img.height, 30)

  /* Display the name of the emote when hovering */
  private val emoteTooltip = new Tooltip(badge.title){
    showDelay = Duration(0)
    hideDelay = Duration(100)
    anchorLocation = PopupWindow.AnchorLocation.WINDOW_BOTTOM_LEFT
  }
  Tooltip.install(node = this, emoteTooltip)
}

object TwitchBadge {
  private val badgeCache = collection.mutable.Map.empty[String, Image]

  def getFromCache(url: String): Image = synchronized {
    if (badgeCache.contains(url)) {
      badgeCache(url)
    } else {
      val img = new Image(url, backgroundLoading = true)
      badgeCache.put(url, img)
      img
    }
  }
}