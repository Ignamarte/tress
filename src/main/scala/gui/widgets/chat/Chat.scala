package gui.widgets.chat

import api.types.collection.TwitchEmotes
import api.types.collection.badges.Badges
import api.types.collection.bttv.{BttvEmote, BttvEmotes}
import api.types.collection.ffz.{FfzEmote, FfzEmotes}
import api.types.{Stream, User}
import com.gikk.twirk.TwirkBuilder
import com.gikk.twirk.enums.{CLEARCHAT_MODE, NOTICE_EVENT}
import com.gikk.twirk.events.TwirkListener
import com.gikk.twirk.types.clearChat.ClearChat
import com.gikk.twirk.types.notice.Notice
import com.gikk.twirk.types.roomstate.Roomstate
import com.gikk.twirk.types.twitchMessage.TwitchMessage
import com.gikk.twirk.types.usernotice.Usernotice
import com.gikk.twirk.types.users.TwitchUser
import com.typesafe.scalalogging.LazyLogging
import gui.widgets.chat.types.ChatRoomstateBuilder
import gui.widgets.control.ClickableButton
import gui.widgets.img.EmoteImg
import gui.widgets.jfx2sfx.FAGlyphIcon
import javafx.scene.robot.Robot
import org.controlsfx.glyphfont.FontAwesome
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.geometry.HPos
import scalafx.geometry.Pos.{Center, TopLeft}
import scalafx.scene.control._
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.Region.USE_PREF_SIZE
import scalafx.scene.layout.{FlowPane, HBox, VBox}
import util.config.UserConfig

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success}

class Chat(stream: Stream)
  extends VBox with LazyLogging {

  private val channel = s"#${stream.user_login.get.toLowerCase}"
  private val nick = User.authenticatedUser.display_name
  private val oauth = f"oauth:${UserConfig.getToken.get}"
  private var autoScrollDisabled: Boolean = false // Control the auto scrolling
  private val MAX_MSG = 150
  private val MAX_SCROLL_MSG = 500

  /*
   * We have to open two IRC connections: One for reading messages and one for
   * sending message. A connection does not receive its own message. That's kind
   * of dirty but avoids a lot of useless parsing of our own messages.
   */
  private var ircChat = createIrcChat
  private var ircChatWrite = createIrcChatWrite

  /* Future to retrieve emotes and badges */
  private val ffzEmotesFuture = FfzEmotes.getEmotes(stream.user_id)
  private val ffzEmotesGlobalFuture = FfzEmotes.getGlobalEmotes
  private val bttvEmotesFuture = BttvEmotes.getEmotes(stream.user_login.getOrElse(""))
  private val bttvEmotesGlobalFuture = BttvEmotes.getGlobalEmotes
  private val twitchEmotesFuture = TwitchEmotes.getEmotes(User.authenticatedUser.id)
  private val chatBadgesFuture = Badges.getBadges(stream.user_id)
  private val generalBadgesFuture = Badges.getGeneralBadges

  /* Style and other properties */
  styleClass.add("chat-container")
  vgrow = Always

  /***********************************/
  /*             MESSAGES            */
  /***********************************/

  /* A list of messages in the chat. Required because chatMessages. children()
     returns a list of Nodes that we cannot cast into a ChatMessage. This list
     is an approximation of what is being displayed and might contain a few
     messages that are not displayed anymore. Synchronising the list is
     more work that it is worth and not an issue unless twitch chats become
     50% notifications.
     This is list is mostly used to check which messages have been deleted
     by a user timeout.
  */
  val messages: ListBuffer[ChatMessage] = ListBuffer[ChatMessage]()
    
  /* The messages displayed in the chat */
  private val chatMessages = new VBox() {
    styleClass.addAll("chat")
    alignment = Center
    vgrow = Always
  }

  private val scrollContainer = new ScrollPane() {
    fitToWidth = true
    fitToHeight = true
    vgrow = Always
    vbarPolicy = ScrollPane.ScrollBarPolicy.Always
    hbarPolicy = ScrollPane.ScrollBarPolicy.Never
    content = chatMessages
  }

  /***********************************/
  /*           NOTIFICATIONS         */
  /***********************************/

  private val notificationPanel = new ChatNotificationPanel

  /***********************************/
  /*           EMOTES INPUT          */
  /***********************************/

  // TODO: Refactoring of this mess

  /* Twitch emotes */
  private lazy val twitchEmotesList = new VBox() {
    styleClass.add("padding-1em")
    spacing = 20
    /* Display the emotes once they are retrieved */
    twitchEmotesFuture.onComplete {
      case Success(e) => Platform.runLater({
        val flowPane = new FlowPane(){
          columnHalignment = HPos.Center
          hgap = 5
          vgap = 5
        }
        e.objects.foreach(emote => {
          // Remove smileys since the api returns regexes as code for those
          if (emote.name.matches("\\w+"))
            flowPane.children.add(
              new EmoteImg(emote.url(), emote.url(size = "3.0"), emote.name, () => appendToInput(emote.name)))
        })
        children.add(flowPane)
      })
      case Failure(_) => Platform.runLater(
        children.add(new Label("Failed to retrieve Twitch emote list"))
      )
    }
  }

  /* FFZ emotes */
  private lazy val ffzEmotesList: VBox = new VBox(){
    styleClass.add("padding-1em")
    spacing = 20
    /* FlowPane for channel specific emotes */
    private val ffzChannelList = new FlowPane() {
      columnHalignment = HPos.Center
      hgap = 5
      vgap = 5
    }
    /* FlowPane for global emotes */
    private val ffzGlobalList = new FlowPane() {
      columnHalignment = HPos.Center
      hgap = 5
      vgap = 5
    }
    children.addAll(ffzChannelList, ffzGlobalList)

    /* Display the emotes once they are retrieved */
    ffzEmotesFuture.onComplete {
      case Success(emotes) => Platform.runLater(
        emotes.foreach(e => ffzChannelList.children.add(new EmoteImg(
          e._2.url(size = "1"),
          e._2.url(size = "4"),
          e._2.name,
          () => appendToInput(e._2.name)
        ))))
      case Failure(_) => Platform.runLater(ffzChannelList.children.add(
        new Label("Failed to retrieve emote list for this channel")
      ))
    }
    ffzEmotesGlobalFuture.onComplete {
      case Success(emotes) => Platform.runLater(
        emotes.foreach(e => ffzGlobalList.children.add(new EmoteImg(
          e._2.url(size = "1"),
          e._2.url(size = "4"),
          e._2.name,
          () => appendToInput(e._2.name)
        ))))
      case Failure(_) => Platform.runLater(ffzGlobalList.children.add(
        new Label("Failed to retrieve global list for this channel")
      ))
    }
  }

  private lazy val bttvEmotesList: VBox = new VBox(){
    styleClass.add("padding-1em")
    spacing = 20
    /* FlowPane for channel specific emotes */
    private val bttvChannelList = new FlowPane() {
      columnHalignment = HPos.Center
      hgap = 5
      vgap = 5
    }
    /* FlowPane for global emotes */
    private val bttvGlobalList = new FlowPane() {
      columnHalignment = HPos.Center
      hgap = 5
      vgap = 5
    }
    children.addAll(bttvChannelList, bttvGlobalList)

    /* Display the emotes once they are retrieved */
    bttvEmotesFuture.onComplete {
      case Success(emotes) => Platform.runLater(
        emotes.foreach(e => bttvChannelList.children.add(new EmoteImg(
          e._2.url(forceGif = true),
          e._2.url(forceGif = true, size = "3x"),
          e._2.code,
          () => appendToInput(e._2.code)
      ))))
      case Failure(_) => Platform.runLater(bttvChannelList.children.add(
          new Label("Failed to retrieve emote list for this channel")
      ))
    }

    bttvEmotesGlobalFuture.onComplete {
      case Success(emotes) => Platform.runLater(
        emotes.foreach(e => bttvGlobalList.children.add(new EmoteImg(
          e._2.url(forceGif = true),
          e._2.url(forceGif = true, size = "3x"),
          e._2.code,
          () => appendToInput(e._2.code)
        ))))
      case Failure(_) => Platform.runLater(bttvGlobalList.children.add(
        new Label("Failed to retrieve global emote list")
      ))
    }
  }

  /* Containers for the emotes */

  private lazy val twitchEmotesScroll = new ScrollPane() {
    styleClass.add("grey-scrollbar")
    fitToWidth = true
    fitToHeight = true
    vbarPolicy = ScrollPane.ScrollBarPolicy.Always
    hbarPolicy = ScrollPane.ScrollBarPolicy.Never
    content = twitchEmotesList
  }

  private lazy val ffzEmotesScroll = new ScrollPane() {
    styleClass.add("grey-scrollbar")
    fitToWidth = true
    fitToHeight = true
    vbarPolicy = ScrollPane.ScrollBarPolicy.Always
    hbarPolicy = ScrollPane.ScrollBarPolicy.Never
    content = ffzEmotesList
  }

  private lazy val bttvEmotesScroll = new ScrollPane() {
    styleClass.add("grey-scrollbar")
    fitToWidth = true
    fitToHeight = true
    vbarPolicy = ScrollPane.ScrollBarPolicy.Always
    hbarPolicy = ScrollPane.ScrollBarPolicy.Never
    content = bttvEmotesList
  }

  private lazy val emotesSelectorTwitchTab = new Tab() {
    text = "Global"
    closable = false
    content = twitchEmotesScroll
  }

  private lazy val emotesSelectorFfzTab = new Tab() {
    text = "FFZ"
    closable = false
    content = ffzEmotesScroll
  }

  private lazy val emotesSelectorBttVTab = new Tab() {
    text = "BTTV"
    closable = false
    content = bttvEmotesScroll
  }

  private lazy val emotesSelectorTabPane = new TabPane() {
    styleClass.add("padding-1em")
    minHeight <== chatInput.height * 5
    prefHeight <== minHeight
    tabs.addAll(
      emotesSelectorTwitchTab,
      emotesSelectorFfzTab,
      emotesSelectorBttVTab
    )
  }

  /***********************************/
  /*            MSG INPUT            */
  /***********************************/

  private val chatInput: TextField = new TextField() {
    hgrow = Always
    promptText = "Enter a message"
    onKeyPressed = (event: KeyEvent) => {
      val sent = event.code match {
        case KeyCode.Enter => sendMsg(this.text.value)
        case KeyCode.Tab => this.requestFocus(); new Robot().keyPress(KeyCode.End); false
        case _ => false
      }
      if (sent) text = ""
    }
  }

  private val emotesBtn: Button = new ClickableButton("", () => displayEmoteSelector()) {
      text
      graphic = FAGlyphIcon(FontAwesome.Glyph.SMILE_ALT.getChar, size = 14)
      prefHeight <== chatInput.height
      prefWidth <== chatInput.height
  }

  private val chatInputContainer: HBox = new HBox(){
    styleClass.add("padding-1em-no-top")
    spacing = 5
    children = List(
      chatInput,
      emotesBtn
    )
  }

  /***********************************/
  /*              METHODS            */
  /***********************************/

  def scrollUp(): Unit =  scrollContainer.vvalue = 0d
  def scrollDown(): Unit =  scrollContainer.vvalue = 1d

  /* BEGINNING OF UTILS METHODS FOR THE FUTURES */

  /**
   * Retrieve the emotes from the bttv future if it's successful
   *
   * @return An optional mapping of BTTV Emotes
   */
  def bttvEmotes: Option[Map[String, BttvEmote]] = {
    if (bttvEmotesFuture.isCompleted && bttvEmotesFuture.value.isDefined)
      bttvEmotesFuture.value.get.toOption
    else
      None
  }

  /**
   * Retrieve the emotes from the bttv global future if it's successful
   *
   * @return An optional mapping of BTTV Emotes
   */
  def bttvGlobalEmotes: Option[Map[String, BttvEmote]] = {
    if (bttvEmotesGlobalFuture.isCompleted && bttvEmotesGlobalFuture.value.isDefined)
      bttvEmotesGlobalFuture.value.get.toOption
    else
      None
  }

  /**
   * Retrieve the emotes from the ffz future if it's successful
   *
   * @return An optional mapping of FFZ Emotes
   */
  def ffzEmotes: Option[Map[String, FfzEmote]] = {
    if (ffzEmotesFuture.isCompleted && ffzEmotesFuture.value.isDefined)
      ffzEmotesFuture.value.get.toOption
    else
      None
  }

  /**
   * Retrieve the emotes from the ffz global future if it's successful
   *
   * @return An optional mapping of FFZ Emotes
   */
  def ffzGlobalEmotes: Option[Map[String, FfzEmote]] = {
    if (ffzEmotesGlobalFuture.isCompleted && ffzEmotesGlobalFuture.value.isDefined)
      ffzEmotesGlobalFuture.value.get.toOption
    else
      None
  }

  /**
   * Retrieve the chat badges future if it's successful
   *
   * @return A `Badges` object
   */
  def chatBadges: Option[Badges] = {
    if (chatBadgesFuture.isCompleted && chatBadgesFuture.value.isDefined) {
      chatBadgesFuture.value.get.toOption
    } else
      None
  }

  /**
   * Retrieve the general badges future if it's successful
   *
   * @return A `Badges` object
   */
  def generalBadges: Option[Badges] = {
    if (generalBadgesFuture.isCompleted && generalBadgesFuture.value.isDefined) {
      generalBadgesFuture.value.get.toOption
    } else
      None
  }

  /* END OF UTILS METHODS FOR THE FUTURES */

  /**
   * Display the emote list
   */
  def displayEmoteSelector(): Unit = {
    if(children.contains(emotesSelectorTabPane))
      children.remove(emotesSelectorTabPane)
    else children.add(1, emotesSelectorTabPane)
  }

  /**
   * Add a text to the chat input, typically an emote code when clicking on it.
   * @param text The string to add
   */
  def appendToInput(text: String): Unit = {
    val chatText = chatInput.text.value
    if (chatText.endsWith(" ") || chatText.isEmpty)
      chatInput.text = chatText + s"${text.strip} "
    else
      chatInput.text = chatText + s" ${text.strip} "
    chatInput.requestFocus()
    chatInput.positionCaret(chatInput.text.value.length)
  }

  /**
   * Send (queue) a twitch chat message
   * @param msg The text message to send
   * @return True if the message has been queued
   */
  def sendMsg(msg: String): Boolean = {
    if (ircChatWrite.isConnected && msg.strip() != "") {
      ircChatWrite.channelMessage(msg) // Send the message
      true
    } else
      false
  }

  /***********************************/
  /*              EVENTS             */
  /***********************************/

  /* Scroll down when a message is added to the chat.
  Binding height change is more reliable since we're sure that the new message is visible
  */
  chatMessages.height.onChange {
    if (!autoScrollDisabled) scrollDown()
  }

  /* Limit to 150 messages */
  chatMessages.children.onChange {
    val overflow = chatMessages.children.length - MAX_MSG
    val overflowMsg = messages.length - MAX_MSG
    if (overflow > 0 && (!autoScrollDisabled || chatMessages.children.length>MAX_SCROLL_MSG))
      chatMessages.children.removeRange(0, overflow)
    if (overflowMsg > 0 && (!autoScrollDisabled || chatMessages.children.length>MAX_SCROLL_MSG))
      messages.remove(0, overflowMsg)
  }

  /**
   * Function to handle incoming message
   *
   * @param sender A Twitch sender
   * @param message A Twitch message
   */
  private def handleMessage(sender: TwitchUser, message: TwitchMessage): Unit = {
    /* Disable auto scrolling if chat is not scrolled to the bottom */
    if (scrollContainer.vvalue.value != 1d)
      autoScrollDisabled = true
    else
      autoScrollDisabled = false
    val isFirstTimeMsg = message.getTagMap.getAsBoolean("first-msg")
    val chatMessage = new ChatMessage(
      sender,
      message.getContent,
      message.getEmotes.asScala.toList,
      message.getCheers.asScala.map(c => c.getMessage -> c).toMap,
      isFirstTimeMsg,
      ffzEmotes,
      ffzGlobalEmotes,
      bttvEmotes,
      bttvGlobalEmotes,
      chatBadges,
      generalBadges
    )
    messages += chatMessage
    Platform.runLater(chatMessages.children.add(chatMessage))
  }

  /**
   * Function to handle incoming notifications
   *
   * @param notice A Twitch Notice
   */
  private def handleNotification(notice: Notice): Unit = {
    notice.getEvent match {
      case NOTICE_EVENT.SLOW_ON => ()
      case NOTICE_EVENT.SLOW_OFF => Platform.runLater(
        notificationPanel.hideSlowMode()
      )
      case NOTICE_EVENT.SUBSCRIBER_MODE_ON => ()
      case NOTICE_EVENT.SUBSCRIBER_MODE_OFF => Platform.runLater(
        notificationPanel.hideSubMode()
      )
      case NOTICE_EVENT.R9K_ON => ()
      case NOTICE_EVENT.R9K_OFF => Platform.runLater(
        notificationPanel.hideR9K()
      )
      /*  Handle the rest */
      case NOTICE_EVENT.EMOTE_ONLY_ON => ()
      case NOTICE_EVENT.EMOTE_ONLY_OFF => Platform.runLater(
        notificationPanel.hideEmotesOnly()
      )
      case _ => notice.getRawNoticeID match { // Cases unhandled by Twirk
        // Ignore notifications handled with the roomstate
        case "followers_on" => ()
        case "followers_on_zero" => ()
        case "followers_off" => ()
        case "msg_banned" => Platform.runLater({
          notificationPanel.displayBanned()
        })
        /* Anything else */
        case _ => Platform.runLater(
          notificationPanel.displayOtherNotification(notice.getMessage)
        )
      }
    }
  }

  /**
   * Display an user notice (subscription raid etc)
   *
   * @param user The user that triggered the notice
   * @param notice The notice
   */
  private def handleNotice(user: TwitchUser, notice: Usernotice): Unit = {
    val sysMsg = notice.getSystemMessage
    val msg = notice.getMessage
    val msgLabel = new Label(sysMsg) {
      styleClass.add("grey")
      hgrow = Always
      wrapText = true
      minHeight = USE_PREF_SIZE
      graphicTextGap = 10
      graphic = FAGlyphIcon(
        FontAwesome.Glyph.INFO_CIRCLE.getChar,
        size = 16
      )
    }
    val msgContainer = new VBox() {
      styleClass.add("padding-0.5em-no-side")
      children = msgLabel
      hgrow = Always
      spacing = 5
    }
    if (!msg.isBlank){
      val chatMessage = new ChatMessage(
        user,
        msg,
        notice.getEmotes.asScala.toList,
        Map(),
        isFirstTimeMsg = false,
        ffzEmotes,
        ffzGlobalEmotes,
        bttvEmotes,
        bttvGlobalEmotes,
        chatBadges,
        generalBadges
      )
      chatMessage.styleClass.addAll("grey", "padding-0.5em")
      msgContainer.children.add(chatMessage)
    }
    Platform.runLater(chatMessages.children.add(msgContainer))
  }

  /**
   * Function to handle incoming clearChat events. Will display the user and
   * time out duration.
   *
   * @param clearChat The clearChat event
   */
  def handleClearChat( clearChat: ClearChat): Unit = {
    if (clearChat.getMode == CLEARCHAT_MODE.USER) {
      val msgLabel: Label = new Label() {
        text = if (clearChat.getDuration == -1) {
          s"${clearChat.getTarget} got permanently banned."
        } else {
          s"${clearChat.getTarget} got timed out for ${clearChat.getDuration}s."
        }
        styleClass.add("grey")
        hgrow = Always
        wrapText = true
        minHeight = USE_PREF_SIZE
        graphicTextGap = 10
        graphic = FAGlyphIcon(
          FontAwesome.Glyph.BAN.getChar,
          size = 16
        )
      }
      val msgContainer = new VBox() {
        styleClass.add("padding-0.5em-no-side")
        children = msgLabel
        hgrow = Always
        spacing = 5
      }
      messages.foreach { msg =>
        if (msg.user.getUserName == clearChat.getTarget)
          Platform.runLater(msg.styleClass.add("deleted"))
      }
      Platform.runLater(chatMessages.children.add(msgContainer))
    }
  }

  /**
   * Function to handle incoming roomstate notifications
   *
   * @param state The state of the chatroom
   */
  private def handleRoomState(state: Roomstate): Unit = {
    val r9kMode = state.get9kMode
    val subMode = state.getSubMode
    val slowTimer = state.getSlowModeTimer
    val emoteMode = state.getEmoteOnlyMode
    val followerMode = state.getFollowersMode

    Platform.runLater({
      if (r9kMode == 1) notificationPanel.displayR9K()
      else if(r9kMode == 0) notificationPanel.hideR9K()

      if (subMode == 1) notificationPanel.displaySubMode()
      else if(subMode == 0) notificationPanel.hideSubMode()

      if (slowTimer > 0) notificationPanel.displaySlowMode(
        s"Slow mode (${slowTimer}s)"
      )
      else if(slowTimer == 0) notificationPanel.hideSubMode()

      if (followerMode > 0) notificationPanel.displayFollowerMode(
        s"Follower Only (${followerMode}min)"
      )
      else if(followerMode == 0) notificationPanel.hideFollowerMode()

      if (emoteMode == 1) notificationPanel.displayEmotesOnly()
      else if(emoteMode == 0) notificationPanel.hideEmotesOnly()
    })
  }

  /* Define what to do when receiving chat related events */
  private def addListeners(): Unit = {

    ircChat.addIrcListener(new TwirkListener() {
      /* Add a ChatMessage to the chat when receiving one*/
      override def onPrivMsg(sender: TwitchUser, message: TwitchMessage): Unit = {
        handleMessage(sender: TwitchUser, message: TwitchMessage)
      }
      /* Display various notifications */
      override def onUsernotice(user: TwitchUser, usernotice: Usernotice): Unit = {
        handleNotice(user, usernotice)
      }
      /* Display timed out users */
      override def onClearChat(clearChat: ClearChat): Unit = {
        handleClearChat(clearChat)
      }
      /* Update notifications based on the roomstate
       * We use a custom type for the RoomState so we have to convert roomstate
       * before feeding it to the handler.
       */
      override def onRoomstate(roomstate: Roomstate): Unit = {
        handleRoomState(roomstate)
      }
      /* Reconnect if twitch asks us to */
      override def onReconnect(): Unit = {
        connect(closeWrite = false)
      }
    })

    ircChatWrite.addIrcListener(new TwirkListener() {
      /* Display incoming notifications */
      override def onNotice(notice: Notice): Unit = {
        handleNotification(notice)
      }
      /* Reconnect if twitch asks us to */
      override def onReconnect(): Unit = {
        connect(closeRead = false)
      }
    })
  }

  /***********************************/
  /*      CONNECTION TO THE CHAT     */
  /***********************************/

  private def createIrcChat = new TwirkBuilder(channel, nick, oauth)
    .setRoomstateBuilder(new ChatRoomstateBuilder)
    .build()

  private def createIrcChatWrite = new TwirkBuilder(channel, nick, oauth).build()

  /*
   * Returns a Future for the async connection to the chat
   * Only reconnects if the connections are closed
   * This function is synchronized to avoid avoid multiple thread closing
   * and opening connections at the same time
   */
  private def makeConnection(
                                closeRead: Boolean = true,
                                closeWrite: Boolean = true
                              ): Unit = this.synchronized{
    Platform.runLater({
      chatMessages.children.clear()
      chatMessages.alignment = Center
      chatMessages.children = new ProgressIndicator()
    })

    if (closeWrite) {
      ircChatWrite.close()
      ircChatWrite = createIrcChatWrite
    }

    if (closeRead) {
      ircChat.close()
      ircChat = createIrcChat
    }

    if (!ircChatWrite.isConnected) {
      logger.info(s"Connecting to $channel (write only)")
      ircChatWrite.connect()
      logger.info(s"Connected to $channel (write only)")
    }

    if (!ircChat.isConnected) {
      logger.info(s"Connecting to $channel")
      ircChat.connect()
      logger.info(s"Connected to $channel")
    }

    Platform.runLater({
      val connectedLabel = new Label("Connected !") {
        graphic = FAGlyphIcon(
          FontAwesome.Glyph.CHECK_CIRCLE_ALT.getChar,
          size = 16
        )}
      chatMessages.children.clear()
      chatMessages.alignment = TopLeft
      chatMessages.children.add(connectedLabel)
    })

    addListeners()
  }


  /**
   * Connect/Reconnect to the chat
   * @param closeRead Whether to close readonly chat connection or not
   * @param closeWrite Whether to close write only chat connection or not
   * @param callback Optional callback to call once the connection is done
   */
  def connect(
               closeRead: Boolean = true,
               closeWrite: Boolean = true ,
               callback: () => Unit = () => ()
             ): Unit = {
    Future[Unit](makeConnection(closeRead, closeWrite)).onComplete {
      case Success(_) => callback()
      case Failure(e) =>
        callback()
        Platform.runLater({
          chatMessages.children.clear()
          chatMessages.children.add(
            new Label(s"Connection Failed, please try again\n(${e.getMessage})"))
        })
    }
  }

  /**
   * Util function to pass `connect` as () => Unit instead of Boolean => Unit
   */
  def reload(callback: () => Unit = () => ()): Unit = {
    notificationPanel.clear()
    connect(callback = callback)
  }

  /**
   * Clear the chat of all its messages
   */
  def clear(): Unit = {
    messages.clear()
    chatMessages.children.clear()
  }

  /* Shutdown the stream and the connection to the chat when called */
  def shutdown(): Unit = {
    new Thread {
      override def run(): Unit = {
        ircChat.disconnect()
        ircChatWrite.disconnect()
      }
    }.start()
  }
  /* Initial connection */
  connect(closeRead= false, closeWrite = false)

  /* Add the children */
  children = List(
    scrollContainer,
    notificationPanel,
    chatInputContainer
  )
}
