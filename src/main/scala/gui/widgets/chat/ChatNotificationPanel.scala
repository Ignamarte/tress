package gui.widgets.chat

import gui.widgets.jfx2sfx.FAGlyphIcon
import javafx.stage.PopupWindow
import org.controlsfx.glyphfont.FontAwesome
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.scene.control.{ContentDisplay, Label, Tooltip}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.Region.USE_PREF_SIZE
import scalafx.scene.layout.{FlowPane, VBox}
import scalafx.util.Duration

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * A VBox displaying notifications for the twitch chat.
 */
class ChatNotificationPanel extends VBox {

  private val TEXT_GAP: Int = 10

  styleClass.add("padding-1em-no-top")
  hgrow = Always
  spacing = 25

  private val container = new VBox() {
    styleClass.add("grey")
  }

  private val permNotifContainer = new FlowPane() {
    hgap = TEXT_GAP
  }

  private val emotesOnlyLabel: Label =  new Label() {
    text = "Emotes only"
    minHeight = USE_PREF_SIZE
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  private val r9kModeLabel: Label =  new Label() {
    text = "R9K mode"
    minHeight = USE_PREF_SIZE
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  private val slowModeLabel: Label =  new Label() {
    minHeight = USE_PREF_SIZE
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  private val subModeLabel: Label =  new Label() {
    text = "Subscriber only"
    minHeight = USE_PREF_SIZE
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  private val followerModeLabel: Label =  new Label() {
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  private val bannedLabel: Label =  new Label() {
    text = "You are permanently banned"
    minHeight = USE_PREF_SIZE
    graphic = FAGlyphIcon(FontAwesome.Glyph.INFO.getChar, size = 16)
    graphicTextGap = TEXT_GAP
    wrapText = true
  }

  /**
   * Create a label that can be removed when clicked on
   *
   * @param msg The message for this label
   * @return A Label
   */
  def buildRemovableLabel(msg: String): Label = {
    new Label(msg) {
      graphic = FAGlyphIcon(FontAwesome.Glyph.CLOSE.getChar, size = 16)
      contentDisplay = ContentDisplay.Right
      wrapText = true
      hgrow = Always
      minHeight = USE_PREF_SIZE
      graphic.value.styleClass.add("clickable")

      private def delete(): Unit = {
        container.children.remove(this)
        updateVisibility()
      }

      /* Remove the notification when the graphic is pressed */
      graphic.value.onMouseClicked = _ => delete()

      /* Automatically remove the notification after a few seconds */
      Future[Unit] {
        Thread.sleep(10000)
        Platform.runLater(delete())
      }
    }
  }

  /**
   * Clear notifications
   */
  def clear(): Unit = {
    permNotifContainer.children.clear()
    container.children.clear()
    updateVisibility()
  }

  /**
   * Hide this notification panel when there is no notification to display
   */
  private def updateVisibility(): Unit = {
    if (permNotifContainer.children.length == 0){
      container.children.remove(permNotifContainer)
    } else {
      if (!container.children.contains(permNotifContainer))
        container.children.add(permNotifContainer)
    }
    if (container.children.length == 0) {
      children.remove(container)
    }
    else {
      if (!children.contains(container)) children.add(container)
    }
  }

  /**
   * Display a notification when the chat is emote only mode
   */
  def displayEmotesOnly(): Unit = {
    if(!permNotifContainer.children.contains(emotesOnlyLabel))
      permNotifContainer.children.add(emotesOnlyLabel)
    updateVisibility()
  }

  /**
   * Display a notification when the chat is in R9K mode
   */
  def displayR9K(): Unit = {
    if(!permNotifContainer.children.contains(r9kModeLabel))
      permNotifContainer.children.add(r9kModeLabel)
    updateVisibility()
  }

  def hideR9K(): Unit = {
    permNotifContainer.children.remove(r9kModeLabel)
    updateVisibility()
  }

  /**
   * Hide the notification when the chat is not in emote only mode
   */
  def hideEmotesOnly(): Unit = {
    permNotifContainer.children.remove(emotesOnlyLabel)
    updateVisibility()
  }

  /**
   * Display a notification when the slow mode is enabled
   *
   * @param msg The message for this notification
   */
  def displaySlowMode(msg: String): Unit = {
    slowModeLabel.text = msg
    if(!permNotifContainer.children.contains(slowModeLabel)) {
      permNotifContainer.children.add(slowModeLabel)
    }
    updateVisibility()
  }

  /**
   * Hide the notification when the slow mode is disabled
   */
  def hideSlowMode(): Unit = {
    permNotifContainer.children.remove(slowModeLabel)
    updateVisibility()
  }

  /**
   * Display a notification when the subscriber mode is enabled
   */
  def displaySubMode(): Unit = {
    if(!permNotifContainer.children.contains(subModeLabel)) {
      permNotifContainer.children.add(subModeLabel)
    }
    updateVisibility()
  }

  /**
   * Hide the notification when the slow mode is disabled
   */
  def hideSubMode(): Unit = {
    permNotifContainer.children.remove(subModeLabel)
    updateVisibility()
  }

  /**
   * Display a notification when the subscriber mode is enabled
   *
   * @param msg The message for this notification
   */
  def displayFollowerMode(msg: String): Unit = {
    followerModeLabel.text = msg
    if(!permNotifContainer.children.contains(followerModeLabel)) {
      permNotifContainer.children.add(followerModeLabel)
    }
    updateVisibility()
  }

  /**
   * Hide the notification when the slow mode is disabled
   */
  def hideFollowerMode(): Unit = {
    permNotifContainer.children.remove(followerModeLabel)
    updateVisibility()
  }

  /**
   * Display a temporary notification that can be hidden by clicking on it
   *
   * @param msg The message for this notification
   */
  def displayOtherNotification(msg: String): Unit = {
    container.children.add(buildRemovableLabel(msg))
    updateVisibility()
  }

  def displayBanned(): Unit = {
    if(!permNotifContainer.children.contains(bannedLabel)) {
      permNotifContainer.children.add(bannedLabel)
    }
    updateVisibility()
  }

  updateVisibility()
}
