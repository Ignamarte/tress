package gui.widgets.chat

import api.types.collection.badges.{Badge, BadgeImage, Badges}
import api.types.collection.bttv.BttvEmote
import api.types.collection.ffz.FfzEmote
import com.gikk.twirk.enums.EMOTE_SIZE
import com.gikk.twirk.types.cheer.{Cheer, CheerSize, CheerTheme, CheerType}
import com.gikk.twirk.types.emote.Emote
import com.gikk.twirk.types.users.TwitchUser
import gui.widgets.img.{EmoteImg, TwitchBadge}
import scalafx.geometry.Pos.Center
import scalafx.scene.control.{Hyperlink, Label, Tooltip}
import scalafx.scene.layout.{FlowPane, HBox, StackPane}
import scalafx.scene.{CacheHint, Node}
import scalafx.util.Duration
import util.Browser

import java.net.URL
import scala.jdk.CollectionConverters._


/**
 * A chat message
 * @param sender The sender of the message
 * @param msgText The message sent by the sender
 * @param msgEmotes The Emotes of the message
 * @param ffzEmotes An optional mapping of FFZ emotes for this channels
 * @param ffzGlobalEmotes An optional of a mapping of global FFZ emotes for this channels
 * @param bttvEmotes An optional a mapping of BBTV emotes for this channels
 * @param bttvGlobalEmotes An optional mapping of global BBTV emotes
 * @param chatBadges An optional list of badges for this channel
 * @param generalBadges An optional list of global badges
 */
class ChatMessage(
                   sender: TwitchUser,
                   msgText: String,
                   msgEmotes: List[Emote],
                   cheers: Map[String, Cheer],
                   isFirstTimeMsg: Boolean,
                   ffzEmotes: Option[Map[String, FfzEmote]],
                   ffzGlobalEmotes: Option[Map[String, FfzEmote]],
                   bttvEmotes: Option[Map[String, BttvEmote]],
                   bttvGlobalEmotes: Option[Map[String, BttvEmote]],
                   chatBadges: Option[Badges],
                   generalBadges: Option[Badges]
                 ) extends FlowPane {

  val user:TwitchUser = sender

  styleClass.add("chat-msg")
  if (isFirstTimeMsg) styleClass.add("first-time")
  cache = true
  cacheHint = CacheHint.Speed

  /**
   * Wrapper for the content of an emote
   * It is required to be able to sort emotes by where they are in the text if
   * that's a twitch emote.
   */
  private case class SimpleEmote(url: String, urlBig: String, start: Int, end: Int, name: String)

  /* The username label for this message */
  private val color: String = Integer.toHexString(sender.getColor)
  private val zeroPadding: String = "0" * (6 - color.length)
  private val userNameLabel: Label = new Label() {
    styleClass.add("chat-username")
    /* Darken a little to avoid invisible (white) usernames */
    style = f"-fx-text-fill: derive(#$zeroPadding$color,-20%%)"
    text = s"${sender.getDisplayName}: "
  }

  /* Context menu for the username label */
  // val contextMenu = new ContextMenu

  /* The list of chat badges for a user */
  private val userBadges: HBox = new HBox {
    alignment = Center

    private val chatBadgesMap= chatBadges.getOrElse(Badges(List())).data
    private val generalBadgesMap = generalBadges.getOrElse(Badges(List())).data
    // Merge the badges maps. General badges are overwritten if needed.
    private val badges_list = generalBadgesMap ++ chatBadgesMap
    private val badges = badges_list.map(badge => badge.set_id -> badge).toMap

    /* Small method to convert a string badge to a tuple */
    private def badgeAsSet(badge: String): (String, String) = {
      val badgeSplit = badge.split("/")
      if (badgeSplit.length == 2) (badgeSplit(0), badgeSplit(1))
      else ("","")
    }

    /* Add the badges */
    sender.getBadges
      .map(badgeAsSet)
      .foreach(badge => {
        val badgeImg: BadgeImage = badges
          .getOrElse(badge._1, Badge.empty).versions.map(img => img.id -> img).toMap
          .getOrElse(badge._2, BadgeImage.empty)
        if (!badgeImg.isEmpty.getOrElse(false)) children.add(buildBadge(badgeImg))
      })
  }

  /**
   * Create an emote as an ImageView from an Emote object.
   * The ImageView is wrapped in a StackPane in order to apply padding to it.
   *
   * @param emote An Emote
   * @return An ImageView a StackPane with the image from the emote
   */
  def buildEmote(emote: SimpleEmote): StackPane = {
    val twitchEmote = new EmoteImg(emote.url, emote.urlBig, emote.name)
    new StackPane(){
      cache = true
      cacheHint = CacheHint.Speed
      styleClass.add("twitch-emote-container")
      children = twitchEmote
    }
  }

  /**
   * Create a badge as an ImageView from an BadgeImage object.
   * The ImageView is wrapped in a StackPane in order to apply padding to it.
   *
   * @param badge A `BadgeImage` object
   * @return An ImageView inside a StackPane with the image from the badge
   */
  def buildBadge(badge: BadgeImage): StackPane = {
    val twitchBadge = new TwitchBadge(badge)
    new StackPane(){
      styleClass.add("twitch-emote-container")
      children = twitchBadge
    }
  }

  /**
   * Build labels based on a provided text
   * Also checks for additional emotes not provided by twitch (FFZ etc) and
   * other specific messages such as link and username highlight
   *
   * @param text The text
   * @return A list of Nodes based on the text
   */
  def buildLabels(text: String): List[Node] = {
    if (text.nonEmpty) {
     text
      .split(" ")
      .map(text => {
        // Check if that's an FFZ Emote
        val ffzEmote = (ffzEmotes.getOrElse(Map()) ++ // Merge the emotes
          ffzGlobalEmotes.getOrElse(Map())).find(emote=>text==emote._1)
        if (ffzEmote.isDefined) {
          val url = ffzEmote.get._2.url()
          val urlBig = ffzEmote.get._2.url(size="4")
          buildEmote(SimpleEmote(url, urlBig, 0, 0, text))
        }
        else {
          // Check if that's a BTTV Emote
          val bttvEmote = (bttvEmotes.getOrElse(Map()) ++ // Merge the emotes
              bttvGlobalEmotes.getOrElse(Map())).find(emote=>text==emote._1)
          if (bttvEmote.isDefined) {
            val emote = bttvEmote.get._2
            val urlBig = emote.url(forceGif = true, size="3x")
            buildEmote(SimpleEmote(emote.url(), urlBig, 0, 0, text))
          }
          else { // That's text, let's check some special cases
          // Highlight @username
          if (text.nonEmpty && text.charAt(0) == '@')
            new Label(s"$text ") {
              styleClass.addAll("bold", "inherit-text-color")
              wrapText = true
            }
          // Display links
          else if (Browser.isUrl(text)) {
            val url = new URL(text)
            val link = new Hyperlink(url.getHost) {
              onMouseClicked = _ => Browser.openURL(url.toString)
            }
            val linkTooltip = new Tooltip(text){
              showDelay = Duration(0)
              hideDelay = Duration(100)
            }
            Tooltip.install(link, linkTooltip)
            link
          }
          // Display bits
          else if (cheers.nonEmpty && cheers.contains(text)) {
            val cheer = cheers(text)
            val cheerType = CheerType.STATIC
            val cheerTheme = CheerTheme.LIGHT
            val cheerSize = CheerSize.TINY
            val cheerImgBaseUrl = cheer.getImageURL(cheerTheme, cheerType, cheerSize)
            val cheerImgUrl = s"https://$cheerImgBaseUrl"
            val icon =
              buildEmote(SimpleEmote(cheerImgUrl, cheerImgUrl, 0, 0, cheer.getMessage))
            val labelText = s"${cheer.getBits.toString} "
            new Label(labelText, icon)
          }
          // Normal text
          else {
            // Cut super long words KEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKWKEKW
            val subText =
              if( text.length > 28) s"${text.substring(0,25)}..."
              else text
            new Label(s"$subText "){styleClass.add("inherit-text-color")}
          }
        }
      }}).toList
    } else List()
  }


  /**
    * Recursively build the message content by removing the emotes name
    * and create the associated images
    *
    * @param text The full text of the message (never changes)
    * @param emotes The list of emotes (never changes)
    * @param offset The offset for the text. Anything before has been processed
    * @param nodes The nodes built from the message before the offset
    * @return A list of Nodes to display the message
    */
  def buildMessage(
                    text: String,
                    emotes: List[SimpleEmote],
                    offset: Int = 0,
                    nodes: List[Node] = List()
                  ): List[Node] = {
    if (emotes.isEmpty)
      nodes ::: buildLabels(text.drop(offset))
    else {
      val emote = emotes.head
      val labels = buildLabels(text.slice(offset, emote.start).strip())
      val imgView = buildEmote(emote)

      buildMessage(text, emotes.tail, emote.end, nodes ::: labels ::: List(imgView))
    }
  }

  /* A sorted and more usable list of emotes in the message */
  private val emoteAsList = msgEmotes.flatMap(
    emote => emote.getIndices.asScala.map(indices => SimpleEmote(
        // Remove this once the library has taken the change into account
        // emote.getEmoteImageUrl(EMOTE_SIZE.SMALL).replace("http://", "https://"),
        "https://static-cdn.jtvnw.net/emoticons/v2/"+emote.getEmoteIDString+"/static/light"+EMOTE_SIZE.SMALL.value, // Not animated as twitch's gifs cause java.io.IOException: Bad GIF LZW: Out-of-sequence code!
        "https://static-cdn.jtvnw.net/emoticons/v2/"+emote.getEmoteIDString+"/static/light"+EMOTE_SIZE.LARGE.value, // Same as above
        indices.beingIndex,
        indices.endIndex,
        emote.getPattern
    ))).sortBy(el => el.start)

  /* Check if message was sent using /me */
  private val message = if (msgText.startsWith("\u0001ACTION")) {
    style = f"-fx-text-fill: derive(#$zeroPadding$color,-20%%)"
    msgText.drop(7).trim
  } else msgText.trim

  /* Display the content of the chat message */
  children.add(userBadges)
  children.add(userNameLabel)
  buildMessage(message, emoteAsList).foreach(children.add(_))

}
