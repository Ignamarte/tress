package gui.widgets.chat.types

import com.gikk.twirk.types.TwitchTags
import com.gikk.twirk.types.roomstate.RoomstateBuilder
import com.gikk.twirk.types.twitchMessage.TwitchMessage

/**
 * A custom ChatRoomState builder
 */
class ChatRoomstateBuilder extends RoomstateBuilder {

  /**
   * Custom builder adding missing information such as emoteMode and
   * FollowerMode.
   *
   * @param message A Twitch Chat message
   * @return A ChatRoomState
   */
  override def build(message: TwitchMessage): ChatRoomState = {

    val rawLine = message.getRaw
    val r = message.getTagMap
    val broadcasterLanguage = Some(r.getAsString(TwitchTags.ROOM_LANG))
    val r9kMode = r.getAsInt(TwitchTags.R9K_ROOM)
    val slowModeTimer = r.getAsInt(TwitchTags.SLOW_DURATION)
    val subMode = r.getAsInt(TwitchTags.SUB_ONLY_ROOM)
    val emoteMode = r.getAsInt("emote-only")
    val followerMode = r.getAsInt("followers-only")

    new ChatRoomState(
      rawLine,
      r9kMode,
      subMode,
      slowModeTimer,
      emoteMode,
      followerMode,
      rawLine
    )
  }
}
