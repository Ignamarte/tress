package gui.widgets.chat.types

import com.gikk.twirk.types.roomstate.Roomstate

/**
 * A custom ChatRoomState
 *
 * @param broadcasterLanguage The broadcaster language
 * @param r9kMode 1 if r9kMode is ON 0 if OFF -1 if not provided
 * @param subMode 1 if subMode is ON 0 if OFF -1 if not provided
 * @param slowModeTimer 1 or more if slowModeTimer is ON 0 if OFF -1 if
 *                      not provided
 * @param emoteMode 1 if emoteMode is ON 0 if OFF -1 if not provided
 * @param followerMode 1 or more if followerMode is ON 0 if OFF -1 if
 *                     not provided
 * @param rawLine The raw message
 */
class ChatRoomState(
                     broadcasterLanguage: String,
                     r9kMode: Int = 0,
                     subMode: Int = 0,
                     slowModeTimer: Int = 0,
                     emoteMode:Int = 0,
                     followerMode: Int = 0,
                     rawLine: String
                   ) extends Roomstate {
  override def getBroadcasterLanguage: String = broadcasterLanguage
  override def get9kMode: Int = r9kMode
  override def getSubMode: Int = subMode
  override def getSlowModeTimer: Int = slowModeTimer
  override def getRaw: String = rawLine
  override def getEmoteOnlyMode: Int = emoteMode
  override def getFollowersMode: Int = followerMode
  def getEmoteMode: Int = emoteMode
  def getFollowerMode: Int = followerMode
}
