package gui.widgets.alert

import gui.stages.MainWindow
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, Label}

class ErrorAlert(message: String, details: String) extends Alert(AlertType.Error) {
  initOwner(MainWindow)
  title = "Error"
  contentText  = message
  dialogPane().setExpandableContent(Label(details))
  showAndWait()
}
