package gui.widgets.cards

import api.types.User
import gui.stages.NotificationStage
import gui.widgets.popover.UserPopover
import gui.widgets.transitions.FadeIn
import javafx.scene.input.MouseButton
import scalafx.geometry.Pos.Center
import scalafx.scene.CacheHint
import scalafx.scene.control.{Label, ProgressIndicator}
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.VBox
import scalafx.stage.Screen
import util.Browser

/**
 * A card for a twitch user.
 * @param user The [[api.types.User]] that needs to be displayed
 * @param notificationWindow An instance of [[gui.stages.NotificationStage]] to
 *                           display the notifications
 * @param followed Whether the user is known to be currently following the
 *                 streamer or not
 */
class UserCard(
                user: User,
                notificationWindow: NotificationStage,
                followed: Boolean = true
              ) extends VBox {

  // The value is also used to make the spinner the same height as a 16:9
  // stream preview
  private val computedPrefWidth = Math.max(
    Screen.primary.bounds.maxX / 20,
    120
  )
  prefWidth = computedPrefWidth
  hgrow = Always
  alignment = Center
  cache = true
  cacheHint = CacheHint.Speed

  private val image = new Image(user.getThumbnail(285, 380), true)
  private val thumbnail =
    new ImageView(image) {
      fitWidth <== width
      preserveRatio = true
      styleClass.add("card-img")
    }

  private val imgProgress = new ProgressIndicator() {
    /* Same height as the stream image to avoid having the user card to "jump"
     * once the image is loaded
     * The images are squares so no need to use a ratio like the stream card
     */
    minHeight = computedPrefWidth
  }
  private val gameName = new Label(user.display_name){styleClass.add("purple-text")}

  children = List(
    imgProgress,
    thumbnail,
    gameName
  )

  private val transition = new FadeIn(600, this)
  transition.play()

  // Context menu
  private lazy val contextMenu = new UserPopover(
    user,
    followed,
    image,
    notificationWindow
  )

  image.progress.onChange(
    (progress,_,_) => if(progress() == 1.0){children.remove(imgProgress)}
  )

  onMousePressed = e => {
    e.getButton match {
      case MouseButton.PRIMARY =>
        val url = f"https://www.twitch.tv/${user.login}"
        Browser.openURL(url)
      case MouseButton.SECONDARY =>
        contextMenu.show(this)
      case _ =>
    }
  }
}
