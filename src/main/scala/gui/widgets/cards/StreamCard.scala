package gui.widgets.cards

import api.types.Stream
import errors.StreamLinkMissingError
import gui.stages.{MainWindow, NotificationStage, StreamWindow}
import gui.widgets.alert.ErrorAlert
import gui.widgets.jfx2sfx.{FAGlyphIcon, Overlay}
import gui.widgets.popover.StreamPopover
import gui.widgets.transitions.FadeIn
import javafx.scene.input.MouseButton
import javafx.stage.{Stage, Window}
import org.controlsfx.glyphfont.FontAwesome
import scalafx.geometry.Pos.Center
import scalafx.scene.CacheHint
import scalafx.scene.control._
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.{HBox, Region, VBox}
import scalafx.stage.Screen
import util.config.UserConfig
import util.{DateUtil, StreamLink}

/**
 * The stream card object. It will launch the stream when left clicked
 * and display a menu when right clicked.
 *
 * @param stream The associated stream
 * @param notificationWindow An instance of [[gui.stages.NotificationStage]] to
 *                           display the notifications
 * @param followed Whether the user is known to be currently following the
 *                 stream or not
 */
class StreamCard(
                  stream: Stream,
                  notificationWindow: NotificationStage,
                  followed: Boolean,
                ) extends VBox {

  // The value is also used to make the spinner the same height as a 16:9
  // stream preview
  private val computedPrefWidth =  Math.max(
    Screen.primary.bounds.maxX / 10,
    250
  )
  prefWidth = computedPrefWidth
  hgrow = Always
  alignment = Center
  cache = true
  cacheHint = CacheHint.Speed

  // Launch the stream
  private def launchStream(): Unit = {
    // Display a notification to the user the user
    MainWindow.displayNotification(
      message = f"Now playing: ${stream.user_name}",
      new ImageView(image){fitWidth = 50; preserveRatio = true}
    )
    try {
      val message = "It looks like streamlink failed to load your stream."
      StreamLink.launch(
        stream.user_login.get,
        _ => new ErrorAlert(message, _) // Display error on failure
      )
      // Retrieve the list of currently opened windows and check if the chat
      // is already open
      val windows = Window.getWindows.toArray().map {
        case stage: Stage => stage.getTitle
        case _ =>
      }
      // Open the chat if it's not already open
      if (UserConfig.getOpenChatWithStream && ! windows.contains(stream.user_name))
        new StreamWindow(stream).show()
    } catch {
      case _:StreamLinkMissingError => new ErrorAlert(
        "StreamLink is missing, please check your configuration.",
        "StreamLink binary is missing from the configuration file."
      )
    }
  }

  // Stream image

  private val image = new Image(stream.getThumbnail(480, 270), true)

  private val thumbnail =
    new ImageView(image) {
      fitWidth <== width
      preserveRatio = true
      styleClass.add("card-img")
  }

  // Stream title overlay

  val overlay = new Overlay(thumbnail, stream.title)

  // Image loading animation

  private val imgProgress = new ProgressIndicator() {
    // Same height as the stream image to avoid having the stream card to "jump"
    // once the image is loaded
    minHeight = computedPrefWidth * StreamCard.PREVIEW_RATIO
  }

  // username, viewer count and uptime

  private val userName = new Label(stream.user_name)
  private val userIcon = FAGlyphIcon(FontAwesome.Glyph.USERS.getChar, size = 14)
  private val uptimeLabel = new Label(DateUtil.uptimeAsString(stream.started_at))
  private val uptimeIcon = FAGlyphIcon(FontAwesome.Glyph.CLOCK_ALT.getChar, size = 14)
  private val hSpace = new Region{hgrow = Always}
  private val viewerCount = Label(f"${stream.viewer_count.toString} ")
  private val topText = new HBox() {
    spacing = 3
    styleClass.add("purple-text-container")
  }

  topText.children = List(
    userName,
    hSpace,
    userIcon,
    viewerCount,
    uptimeIcon,
    uptimeLabel
  )

  // Add nodes

  children = List(
    topText,
    imgProgress,
    overlay
  )

  private val transition = new FadeIn(600, this)
  transition.play()

  // Context Menu

  private lazy val contextMenu = new StreamPopover(
    stream,
    followed,
    image,
    notificationWindow
  )

  // Events

  image.progress.onChange(
    (progress,_,_) => if(progress() == 1.0){
      children.remove(imgProgress)
      cache = true
    }
  )

  overlay.onMouseClicked = e => {
    e.getButton match {
      case MouseButton.PRIMARY => launchStream()
      case MouseButton.SECONDARY => contextMenu.show(overlay)
      case _ =>
    }
  }
}

/**
 * Companion objects used to hold static values
 */
object StreamCard {
  private val PREVIEW_RATIO = 9f/16f
}