package gui.widgets.cards

import api.types.Game
import gui.panels.MainPanel
import gui.widgets.transitions.FadeIn
import scalafx.geometry.Pos.Center
import scalafx.scene.{CacheHint, Node}
import scalafx.scene.control.{Label, ProgressIndicator}
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.layout.Priority.Always
import scalafx.scene.layout.VBox
import scalafx.stage.Screen

/**
 * The stream game object. It will open the game's stream list when left clicked
 *
 * @param game The associated game
 * @param parentPanel The parent node (Panel) that will be displayed again if
 *                    the "go back" button is pressed
 */
class GameCard(game: Game, parentPanel: Node) extends VBox {

  private val computedPrefWidth = Math.max(
    Screen.primary.bounds.maxX / 20,
    150
  )
  prefWidth = computedPrefWidth
  hgrow = Always
  alignment = Center
  cache = true
  cacheHint = CacheHint.Speed

  private val image = new Image(game.getThumbnail(GameCard.WIDTH, GameCard.HEIGHT), true)
  private val thumbnail =
    new ImageView(image) {
      fitWidth <== width
      preserveRatio = true
      styleClass.add("card-img")
    }

  private val imgProgress = new ProgressIndicator(){
    // Same height as the stream image to avoid having the game card to "jump"
    // once the image is loaded
    minHeight = computedPrefWidth * GameCard.PREVIEW_RATIO
  }
  private val gameName = new Label(game.name){styleClass.add("purple-text")}

  children = List(
    imgProgress,
    thumbnail,
    gameName
  )

  private val transition = new FadeIn(600, this)
  transition.play()

  image.progress.onChange(
    (progress,_,_) => if(progress() == 1.0){children.remove(imgProgress)}
  )

  onMousePressed = e =>
    if (e.isPrimaryButtonDown) {
      MainPanel.displayStreams(Some(game.id), game.name, parentPanel)
    }
}

object GameCard {
  private val HEIGHT = 380
  private val WIDTH = 285
  private val PREVIEW_RATIO = HEIGHT.toFloat / WIDTH.toFloat
  println(PREVIEW_RATIO)
}