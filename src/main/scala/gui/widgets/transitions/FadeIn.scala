package gui.widgets.transitions

import scalafx.animation.FadeTransition
import scalafx.scene.Node
import scalafx.util.Duration

class FadeIn(duration: Int, el: Node)
  extends FadeTransition(Duration(duration), el) {
  fromValue = 0
  toValue = 1
}
