package gui.widgets.control

import gui.widgets.jfx2sfx.FAGlyphIcon
import org.controlsfx.glyphfont.FontAwesome
import scalafx.beans.property.ReadOnlyDoubleProperty
import scalafx.scene.control.Button

class MenuButton(
                  label: String,
                  width: ReadOnlyDoubleProperty,
                  action: () => Unit,
                ) extends Button(label) {
  prefWidth <== width
  styleClass.add("menu-btn")

  onMouseClicked = _ => action()
  graphic = FAGlyphIcon(FontAwesome.Glyph.CHEVRON_RIGHT.getChar, size = 10)
}
