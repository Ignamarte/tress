package gui.widgets.control

import scalafx.geometry.Pos.Center
import scalafx.scene.control.Label

class Title(title:String) extends Label(title){
  alignment = Center
  styleClass.add("title")
}
