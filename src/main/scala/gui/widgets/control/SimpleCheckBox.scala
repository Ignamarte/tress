package gui.widgets.control

import gui.stages.MainWindow
import scalafx.scene.control.CheckBox

class SimpleCheckBox(checked: Boolean, callback: Boolean => Unit)
  extends CheckBox {

  styleClass.add("purple-checkbox")

  selected = checked

  onAction = _ => {
    callback(selected.value)
    MainWindow.displayNotification("Settings saved")
  }
}
