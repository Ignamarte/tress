package gui.widgets.control

import javafx.scene.input.MouseButton
import scalafx.scene.control.Button

class ClickableButton(label: String, action: () => Unit) extends Button(label) {
  styleClass.addAll("grey")
  onMouseClicked = event => if (event.getButton == MouseButton.PRIMARY) action()
}
