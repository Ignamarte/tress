package gui.widgets.control

import gui.stages.MainWindow
import scalafx.scene.control.Button
import scalafx.stage.FileChooser

class FileDialogButton(
                        dialogTitle: String,
                        text: Option[String],
                        callback: String => Unit
                      ) extends Button(text.getOrElse("No file selected")) {

  styleClass.add("grey")

  private val fileChooser: FileChooser = new FileChooser{
    title = dialogTitle
  }
  onMouseClicked = _ => {
    val selectedFile = fileChooser.showOpenDialog(MainWindow)
    if (selectedFile != null) {
      text_=(selectedFile.getCanonicalPath)
      callback(selectedFile.getCanonicalPath)
      MainWindow.displayNotification("Settings saved")
    }
  }
}
