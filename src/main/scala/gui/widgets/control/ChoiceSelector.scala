package gui.widgets.control

import gui.stages.MainWindow
import scalafx.collections.ObservableBuffer
import scalafx.scene.control.ChoiceBox

class ChoiceSelector[T](
                         items: ObservableBuffer[T],
                         selected: Option[T],
                         callback: T => Unit
                       ) extends ChoiceBox[T](items){
  styleClass.add("grey")

  selected match {
    case Some(s) => value = s
    case None =>
  }

  onAction = _ => {
    callback(value.getValue)
    MainWindow.displayNotification("Settings saved")
  }
}
