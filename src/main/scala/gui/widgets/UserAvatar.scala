package gui.widgets

import gui.widgets.popover.LoggedUserPopover
import javafx.scene.input.MouseButton
import javafx.scene.paint.ImagePattern
import scalafx.scene.image.Image
import scalafx.scene.shape.Circle

class UserAvatar extends Circle {

  styleClass.add("avatar")

  val defaultImg = "images/twitch_icon.png"
  private val userMenu = new LoggedUserPopover()

  def update(url: String, username: String): Unit = {
    this.setFill(new ImagePattern(new Image(url)))
    userMenu.updateLabel(username)
  }

  onMouseClicked = event => {
    println(this.fill.value)
    event.getButton match {
      case MouseButton.PRIMARY | MouseButton.SECONDARY => userMenu.show(this)
      case _ =>
    }
  }
}
