package gui.widgets.jfx2sfx

import org.controlsfx.control.InfoOverlay
import scalafx.scene.Node

class Overlay(override val delegate: InfoOverlay = new InfoOverlay)
  extends Node(delegate) {

  def this(node: Node, text: String) {
    this(new InfoOverlay(node, text))
  }

}

object Overlay {
  def apply(node: Node, text: String) = new Overlay(node, text)
}
