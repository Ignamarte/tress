package gui.widgets.jfx2sfx

import org.controlsfx.glyphfont.{FontAwesome, Glyph}
import scalafx.scene.Node

class FAGlyphIcon(override val delegate: Glyph = new Glyph)
  extends Node(delegate) {

  def this(symbol: Char, size: Int) {
    this(FAGlyphIcon.fontAwesome.create(symbol).size(size))
  }

  def this(symbol: Char) {
    this(symbol, 12)
  }
}

object FAGlyphIcon {
  private val file = getClass.getResourceAsStream("/font/fa5-solid.otf")
  private val fontAwesome = new FontAwesome(file)

  def apply(symbol: Char) = new FAGlyphIcon(symbol)

  def apply(symbol: Char, size: Int) = new FAGlyphIcon(symbol, size)
}
