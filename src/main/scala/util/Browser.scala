package util

import java.util.regex.Pattern

// TODO: Do something when browsing isn't supported
object Browser {

  private val urlRegex =
    "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
  private val regexPattern = Pattern.compile(urlRegex)

  def openURL(url: String, parameters: Map[String, String] = Map()): Unit = {
    val completeURL =
      if(parameters.nonEmpty) {
        url + parameters.map(_.productIterator.mkString("=")).mkString("?","&","")
      } else
        url

    tress.Tress.hostServices.showDocument(completeURL)
  }

  def isUrl(string: String): Boolean = {
    val matcher = regexPattern.matcher(string)
    matcher.matches
  }
}
