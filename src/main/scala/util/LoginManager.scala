package util

import api.RESTClient
import api.types.User
import api.webserver.WebServer
import gui.panels.{MainPanel, SideMenu}
import gui.stages.MainWindow
import scalafx.application.Platform
import util.config.APIConfig

object LoginManager {

  private val oauthURL = APIConfig.getOauthURL
  private val oauthResponseType = APIConfig.getOauthResponseType
  private val oauthRedirectURL = APIConfig.getOauthRedirectURL
  private val oauthScopes = APIConfig.getOauthScopes
  private val clientID = APIConfig.getClientID

  private var callback: () => Unit = () => {}

  def startOAuth(callback: () => Unit): Unit = {

    this.callback = callback

    val parameters = Map(
      "client_id" -> clientID,
      "redirect_uri" -> oauthRedirectURL,
      "response_type" -> oauthResponseType,
      "scope" -> oauthScopes.mkString("+")
    )
    WebServer.init()
    Browser.openURL(oauthURL, parameters)
  }

  def applyCallback(): Unit = {
    Platform.runLater(MainWindow.toFront())
    Platform.runLater(callback())
  }

  def logoff(): Unit = {
    new Thread {
      override def run() {
        User.clearCache()
        RESTClient.revokeToken
        SideMenu.updateAvatar()
        Platform.runLater(
          MainPanel.displayLoginPanel(() => MainPanel.displayFollowsPanel())
        )
      }
    }.start()
  }

}
