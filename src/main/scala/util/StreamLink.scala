package util

import errors.StreamLinkMissingError
import scalafx.application.Platform
import util.config.UserConfig

import scala.collection.immutable.ListMap
import scala.sys.process._

object StreamLink {

  val twitch = "twitch.tv/"
  val defaultQuality = "best"
  val defaultQualityExclusion = "None"

  // Hack to get the stream quality based on keywords. Huge thanks to a randomly
  // found comment in the streamlink-twitch-gui app source code.
  private val qualityExclusion = ListMap(
    "best" -> defaultQualityExclusion,
    "very high" -> ">720p60",
    "high" -> ">720p30",
    "medium" -> ">540p30",
    "low" -> ">360p30",
    "mobile" -> ">160p30",
    "audio_only"-> defaultQualityExclusion
  )

  def getStreamQualities: List[String] = {
    qualityExclusion.map({case (key, _: Any) => key}).toList
  }

  /**
   * Starts a stream by calling streamlink
   *
   * @param user_login The login of the twitch user
   * @param onRuntimeErrorCallback Callback if launching the stream fails
   */
  def launch(user_login: String, onRuntimeErrorCallback: String => Unit): Unit = {
    val streamlink = UserConfig.getStreamLink
    val player = UserConfig.getPlayer

    if (streamlink.isEmpty) {
      throw new StreamLinkMissingError()
    }

    val quality = UserConfig.getQuality.getOrElse(defaultQuality)

    // Part of the hack, we exclude any stream that's "too good" using
    // --stream-sorting-excludes and take the best of what remains,
    // or the worst of what's been excluded if nothing remains.
    val fakeQuality = qualityExclusion.getOrElse(quality, defaultQuality) match {
      case `defaultQualityExclusion` => quality
      case _:String => "best, worst-unfiltered, best-unfiltered"
    }

    val playerOption = player match {
      case Some(s:String) => Seq("--player",s)
      case None => Seq()
    }

    val command =
      Seq(streamlink.get) ++
      playerOption ++
      Seq(
        "--stream-sorting-excludes",
        qualityExclusion(quality),
        twitch+user_login,
        fakeQuality
      )

    // Start the stream and return the output stream using the provided
    // callback function in case streamlink fails
    new Thread {
      override def run(): Unit = {
        val os = new java.io.ByteArrayOutputStream
        try {
          val exitCode = (command #> os).!
          if (exitCode != 0) {
            Platform.runLater(onRuntimeErrorCallback(os.toString))
          }
        } catch {
          case e:RuntimeException =>
            val msg = f"Couldn't start streamlink, check your settings.\n\n$e"
            Platform.runLater(onRuntimeErrorCallback(msg))
        }
      }
    }.start()

  }

}
