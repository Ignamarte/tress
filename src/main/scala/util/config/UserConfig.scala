package util.config

import java.io.PrintWriter
import java.nio.file.{Files, Path, Paths}

import errors.APIConfigError
import pureconfig.{ConfigSource, ConfigWriter}
import pureconfig.generic.auto._

object UserConfig {

  private case class Config (
                    token: Option[String] = None,
                    player: Option[String] = None,
                    streamlink: Option[String] = None,
                    quality: Option[String] = None,
                    height: Option[Double] = None,
                    width: Option[Double] = None,
                    maximized: Option[Boolean] = None,
                    refreshFollows: Boolean = true,
                    disableGifEmotes: Boolean = false,
                    openChatWithStream: Boolean = false
                    )

  private val file = "user.conf"
  private val path: Path = Paths.get(file)

  private var conf: Config = if (Files.exists(path)) {
    ConfigSource.default(ConfigSource.file(path)).load[Config] match {
      case Right(c) => c
      case Left(e) => throw new APIConfigError(e.toString)
    }
  } else {
    Config()
  }

  def getToken: Option[String] = {
    conf.token
  }

  def getStreamLink: Option[String] = {
    conf.streamlink
  }

  def getPlayer: Option[String] = {
    conf.player
  }

  def getQuality: Option[String] = {
    conf.quality
  }

  def getHeight: Option[Double] = {
    conf.height
  }

  def getWidth: Option[Double] = {
    conf.width
  }

  def getIsMaximized: Option[Boolean] = {
    conf.maximized
  }

  def getRefreshFollows: Boolean = {
    conf.refreshFollows
  }

  def getDisableGifEmotes: Boolean = {
    conf.disableGifEmotes
  }

  def getOpenChatWithStream: Boolean = {
    conf.openChatWithStream
  }

  def setToken(token: String): Unit = {
    conf = conf.copy(token = Some(token))
    saveConf()
  }

  def setStreamLink(link: String): Unit = {
    conf = conf.copy(streamlink = Some(link))
    saveConf()
  }

  def setPlayer(link: String): Unit = {
    conf = conf.copy(player = Some(link))
    saveConf()
  }

  def setQuality(quality: String): Unit = {
    conf = conf.copy(quality = Some(quality))
    saveConf()
  }

  def setHeight(height: Double): Unit = {
    conf = conf.copy(height = Some(height))
    saveConf()
  }

  def setWidth(width: Double): Unit = {
    conf = conf.copy(width= Some(width))
    saveConf()
  }

  def setMaximized(maximized: Boolean): Unit = {
    conf = conf.copy(maximized = Some(maximized))
    saveConf()
  }

  def setRefreshFollows(refresh: Boolean): Unit = {
    conf = conf.copy(refreshFollows = refresh)
    saveConf()
  }

  def setDisableGifEmotes(disable: Boolean): Unit = {
    conf = conf.copy(disableGifEmotes = disable)
    saveConf()
  }

  def setOpenChatWithStream(open: Boolean): Unit = {
    conf = conf.copy(openChatWithStream = open)
    saveConf()
  }

  private def saveConf(): Unit = {
    val writer = ConfigWriter[Config].to(conf)
    val out = new PrintWriter(file)
    try {
      out.write(writer.render())
    } finally {
      out.close()
    }
  }

}
