package util.config

import pureconfig.generic.auto._
import errors.APIConfigError
import pureconfig.ConfigSource

object APIConfig {

  private case class APIParams (
                         clientID: String,
                         oauthScopes: List[String],
                         oauthBaseURL: String,
                         oauthRedirectURL: String,
                         oauthResponseType: String,
                         oauthValidateURL: String,
                         oauthRevokeURL: String,
                         streamsEndpoint: String,
                         topGamesEndpoint: String,
                         gamesEndpoint: String,
                         userSubscriptionsEndpoint: String,
                         userDetailsEndpoint: String,
                         addFollowEndpoint: String,
                         searchGamesEndpoint: String,
                         searchChannelsEndpoint: String,
                         userEmotesEndpoint: String,
                         ffzRoomEndpoint: String,
                         ffzGlobalEndpoint: String,
                         chatBadgeEndpoint: String,
                         generalBadgeEndpoint: String,
                         bttvGeneralEndpoint: String,
                         bttvRoomEndpoint: String,
                         bttvEmoteUrl: String,
                         bttvEmoteUrlStatic: String,
                         twitchEmoteUrl: String,
                         defaultItemCount: Int,
                         maxIDCount: Int
                       )
  private case class Config(api: APIParams)
  private val conf: Config = ConfigSource.default.load[Config] match {
    case Right(c) => c
    case Left(e) => throw new APIConfigError(e.toString)
  }

  def getOauthValidateURL: String = conf.api.oauthValidateURL
  def getOauthRevokeURL: String = conf.api.oauthRevokeURL
  def getOauthURL: String = conf.api.oauthBaseURL
  def getOauthResponseType: String = conf.api.oauthResponseType
  def getOauthRedirectURL: String = conf.api.oauthRedirectURL
  def getOauthScopes: List[String] = conf.api.oauthScopes
  def getClientID: String = conf.api.clientID

  def getStreamsEndPoint: String = conf.api.streamsEndpoint
  def getTopGamesEnPoint: String = conf.api.topGamesEndpoint
  def getGamesEndPoint: String = conf.api.gamesEndpoint
  def getUserDetailsEndpoint: String = conf.api.userDetailsEndpoint
  def getUserSubscriptionsEndpoint: String = conf.api.userSubscriptionsEndpoint
  def getAddFollowEndpoint: String = conf.api.addFollowEndpoint
  def getSearchGamesEndPoint: String = conf.api.searchGamesEndpoint
  def getSearchChannelEndPoint: String = conf.api.searchChannelsEndpoint

  def getChatBadgeEndpoint: String = conf.api.chatBadgeEndpoint
  def getGeneralBadgeEndpoint: String = conf.api.generalBadgeEndpoint

  def getFfzRoomEndPoint: String = conf.api.ffzRoomEndpoint
  def getFfzGlobalEndpoint: String = conf.api.ffzGlobalEndpoint

  def getUserEmotesEndpoint: String = conf.api.userEmotesEndpoint
  def getBttvGeneralEndpoint: String = conf.api.bttvGeneralEndpoint
  def getBttvRoomEndpoint: String = conf.api.bttvRoomEndpoint
  def getBttvEmoteUrl: String = conf.api.bttvEmoteUrl
  def getBttvEmoteUrlStatic: String = conf.api.bttvEmoteUrlStatic
  def getTwitchEmoteUrl: String = conf.api.twitchEmoteUrl

  def getDefaultMaxItemCount: Int = conf.api.defaultItemCount
  def getmaxIDCount: Int = conf.api.maxIDCount
}
