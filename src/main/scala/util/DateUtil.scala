package util

import org.joda.time.{DateTime, Minutes}
import org.joda.time.format.ISODateTimeFormat

/**
 * Util class to manage, parse and convert dates
 */
object DateUtil {
    private val timePatern = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private val parser = ISODateTimeFormat.dateTimeParser()

  /**
   * Parse a date and return a string with the duration between the date and now
   * @param startedAt The date to parse and compare
   * @return An uptime string as follow: [hours]:[minutes]
   */
    def uptimeAsString(startedAt: String): String = {
      val startedAtDate: DateTime = parser.parseDateTime(startedAt)
      val totalMinutes = Minutes.minutesBetween(startedAtDate, new DateTime())
        .getMinutes
      val hours = totalMinutes / 60
      val minutes = totalMinutes - hours * 60
      s"$hours:${ if (minutes < 10) "0" else "" }$minutes"
    }
}
