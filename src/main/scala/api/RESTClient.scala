package api

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

import api.types.Pagination
import errors.{APIConfigError, APIError}
import util.config.{APIConfig, UserConfig}

import scala.collection.mutable.ListBuffer

object RESTClient {

  private var oauthToken = UserConfig.getToken.getOrElse("")

  private val oauthValidateURL: String = APIConfig.getOauthValidateURL
  private val oauthRevokeURL: String = APIConfig.getOauthRevokeURL
  private val clientID: String = APIConfig.getClientID

  private val StreamsEndPoint: String = APIConfig.getStreamsEndPoint
  private val TopGamesEnPoint: String = APIConfig.getTopGamesEnPoint
  private val GamesEndPoint: String = APIConfig.getGamesEndPoint
  private val UserDetailsEndpoint: String = APIConfig.getUserDetailsEndpoint
  private val UserSubscriptionsEndPoint: String = APIConfig.getUserSubscriptionsEndpoint
  private val AddFollowEndPoint: String = APIConfig.getAddFollowEndpoint
  private val searchGamesEndPoint: String = APIConfig.getSearchGamesEndPoint
  private val searchChannelsEndPoint: String = APIConfig.getSearchChannelEndPoint

  private val userEmotesEndpoint = APIConfig.getUserEmotesEndpoint
  private val ffzRoomEndPoint: String = APIConfig.getFfzRoomEndPoint
  private val ffzGlobalEndpoint: String = APIConfig.getFfzGlobalEndpoint
  private val bbtvGeneralEndpoint: String = APIConfig.getBttvGeneralEndpoint
  private val bbtvRoomEndpoint: String = APIConfig.getBttvRoomEndpoint

  private val chatBadgeEndpoint: String = APIConfig.getChatBadgeEndpoint
  private val generalBadgeEndpoint: String = APIConfig.getGeneralBadgeEndpoint

  private val defaultMaxItemCount: Int = APIConfig.getDefaultMaxItemCount

  def setToken(token: String): Unit = {
    oauthToken = token
    UserConfig.setToken(token)
  }

  private def defaultHeader(auth: String): Map[String, String] = {
    Map(
      "Client-ID" -> clientID,
      "Authorization" -> s"$auth $oauthToken"
    )
  }

  @scala.annotation.tailrec
  private def sendRequest(
                             url: String,
                             method: String,
                             header:  Map[String, String] = Map(),
                             params: List[(String, String)] = List(),
                             retry: Int = 5
                           ):(Int, String) = {

    val requestMethod = method match {
      case "GET" => requests.get
      case "POST" => requests.post
      case "PUT" => requests.put
      case "DELETE" => requests.delete
      case _ => throw new APIConfigError(f"Invalid method: $method")
    }

    // Send the request and retry if the request failed and retry != 0
    try {
      val r = requestMethod(
        url,
        headers = header,
        params = params
      )

      // Should be useless with new version of requests, but just in case ...
      if (r.statusCode < 200 || r.statusCode >= 300) {
        throw APIError(r.statusCode, r.text)
      }
      // RETURN VALUES
      (r.statusCode, r.text)
    }
    catch {
      case e:Throwable =>
        if (retry > 0) {
          Thread.sleep(200)
          sendRequest(url,method,header,params,retry-1)
        } else throw e
    }
  }

  private def sendTwitchReq(
                           endPoint: String,
                           method: String,
                           params: ListBuffer[(String, String)],
                           pagination: Option[Pagination],
                           auth: String = "Bearer",
                           maxLength: Int = defaultMaxItemCount
                         ): (Int, String) = {

    pagination match {
      case Some(Pagination(page, cursor)) => params += ((page, cursor))
      case _ =>
    }

    params += (("first", maxLength.toString))

    val paramsList = params.toList
    val header = defaultHeader(auth)

    sendRequest(endPoint, method, header, paramsList)
  }

  def getTopStreams(
                     gameID: Option[String],
                     pagination: Option[Pagination],
                     users: Option[List[String]]
                   ): (Int, String) = {

    val params = ListBuffer[(String, String)]()

    if (gameID.isDefined) params += (("game_id",gameID.get))
    if (users.isDefined) users.get.foreach(u => params += (("user_id", u)))
    sendTwitchReq(StreamsEndPoint, "GET", params, pagination)
  }

  def getStream(id: String): (Int, String) = {
    val params = ListBuffer(("user_id", id))
    sendTwitchReq(StreamsEndPoint, "GET", params, None)
  }

  def searchChannels(query: String, page: Option[Pagination]): (Int, String) = {
    val params = ListBuffer[(String, String)]()
    val encodedQuery = URLEncoder.encode(query, StandardCharsets.UTF_8.toString)
    val url = searchChannelsEndPoint.replace("<query>", encodedQuery)
    sendTwitchReq(url, "GET", params, None)
  }

  def getTopGames(page: Option[Pagination]): (Int, String) = {
    val params = ListBuffer[(String, String)]()
    sendTwitchReq(TopGamesEnPoint, "GET", params, page)
  }

  def getGames(ids: List[String], page: Option[Pagination]):
  (Int, String) = {
    val params = ListBuffer[(String, String)]()
    ids.foreach(u => params += (("id", u)))
    sendTwitchReq(GamesEndPoint, "GET", params, page)
  }

  def searchGames(query: String, page: Option[Pagination]): (Int, String) = {
    val params = ListBuffer[(String, String)]()
    val encodedQuery = URLEncoder.encode(query, StandardCharsets.UTF_8.toString)
    val url = searchGamesEndPoint.replace("<query>", encodedQuery)
    sendTwitchReq(url, "GET", params, page)
  }

  def getSubscriptions(
                        id: String,
                        page: Option[Pagination]
                      ): (Int, String) = {

    val params = ListBuffer(("user_id", id))
    sendTwitchReq(UserSubscriptionsEndPoint, "GET", params, page)
  }

  /**
   * Send a request to check if a user follow another user
   *
   * @param from_id The ID of the user that follows
   * @param to_id The ID of the user that is followed
   * @return A tuple with the return status code and the response body (json)
   */
  def getSubscription(
                        from_id: String,
                        to_id: String,
                      ): (Int, String) = {

    val params = ListBuffer(("from_id", from_id), ("to_id", to_id))
    sendTwitchReq(UserSubscriptionsEndPoint, "GET", params, None)
  }

  def getUsersDetails(
                       ids: Option[List[String]],
                       limit: Int = defaultMaxItemCount): (Int, String) = {
    val params = ListBuffer[(String, String)]()
    if(ids.isDefined) ids.get.foreach(u => params += (("id", u)))
    sendTwitchReq(UserDetailsEndpoint, "GET", params, None, maxLength = limit)
  }

  def editFollow(userID: String, channelID: String, delete: Boolean): Unit = {
    val url = AddFollowEndPoint
      .replace("<user ID>", userID)
      .replace("<channel ID>", channelID)

    val method = if (delete) "DELETE" else "PUT"

    val params = ListBuffer(("api_version", "5"))

    sendTwitchReq(url, method, params, None, "OAuth")
  }

  def validateUser: (Int, String) = {
    sendTwitchReq(
      oauthValidateURL,
      "GET",
      ListBuffer[(String, String)](),
      None,
      "OAuth"
    )
  }

  def revokeToken: (Int, String) = {
    val params = ListBuffer(("client_id", clientID),("token", oauthToken))
    sendTwitchReq(oauthRevokeURL, "POST", params, None, "OAuth")
  }

  def getGlobalEmotes(id: String): (Int, String) = {
    val params = ListBuffer[(String, String)]()
    sendTwitchReq(userEmotesEndpoint, "GET", params, None)
  }

  def getFfzRoom(id: String): (Int, String) = {
    val url = ffzRoomEndPoint.replace("<id>", id)
    sendRequest(url, "GET")
  }

  def getFfzGlobal: (Int, String) = {
    val url = ffzGlobalEndpoint
    sendRequest(url, "GET")
  }

  def getBttvGlobal: (Int, String) = {
    val url = bbtvGeneralEndpoint
    sendRequest(url, "GET")
  }

  def getBttvRoom(id: String): (Int, String) = {
    val url = bbtvRoomEndpoint.replace("<id>", id)
    sendRequest(url, "GET")
  }

  def getChatBadges(id: String): (Int, String) = {
    val url = chatBadgeEndpoint.replace("<channel ID>", id)
    val params = ListBuffer(("broadcaster_id", id))
    sendTwitchReq(url, "GET", params, None)
  }

  def getBadges: (Int, String) = {
    val params = ListBuffer[(String, String)]()
    sendTwitchReq(generalBadgeEndpoint, "GET", params, None)
  }
}
