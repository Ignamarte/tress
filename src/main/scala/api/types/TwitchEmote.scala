package api.types

import util.config.APIConfig

case class TwitchEmote(
                        id: String,
                        name: String
                      ) {

  private val sizes = List[String]("1.0", "2.0", "3.0")

  def url(size: String = "1.0"): String = {
    val sizeWithDefault =
      if (sizes.contains(size)) size
      else sizes.head
    APIConfig.getTwitchEmoteUrl
      .replace("<id>", id)
      .replace("<size>", sizeWithDefault)
  }
}
