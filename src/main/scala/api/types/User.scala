package api.types

import java.time._

import api.RESTClient
import api.types.collection.Users
import errors.NotLoggedInError
import requests.RequestFailedException
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class User (
                  login: String,
                  id: String,
                  display_name: String,
                  profile_image_url: String
                )
{

  def getThumbnail(width: Int, height: Int): String = {
    this.profile_image_url
      .replace("{width}", width.toString)
      .replace("{height}", height.toString)
  }

  def follow(channelID: String): Future[Unit] = Future[Unit] {
    RESTClient.editFollow(id, channelID, delete = false)
  }

  def unfollow(channelID: String): Future[Unit] = Future[Unit] {
    RESTClient.editFollow(id, channelID, delete = true)
  }
}

object User extends APIObject[User] {

  protected val decoder: Reads[User] = Json.reads[User]
  private var locked: Boolean = false
  private var cachedUser: Option[User] = None
  private var lastValidationDate = LocalDateTime.MIN


  def clearCache(): Unit = {
    cachedUser = None
  }

  def authenticatedUser: User = {

    while (locked) {
      Thread.sleep(50)
    }

    locked = true

    val delta = Duration.between(
      lastValidationDate,
      LocalDateTime.now
    )

    if (delta.toMinutes > 60) {
      clearCache()
    }

    cachedUser match {
      case Some(user) =>
        locked = false
        user
      case None =>
        try {
          RESTClient.validateUser
          cachedUser =  Some(Users.getAuthenticatedUser)
          lastValidationDate = LocalDateTime.now
          cachedUser.get
        } catch {
          case e:RequestFailedException =>
            if (e.response.statusCode == 401) throw new NotLoggedInError
            else throw e
        } finally {
          locked = false
        }
    }
  }
}
