package api.types

/**
 *
 * A twitch channel (~=user)
 *
 * @param broadcaster_login The login of the user
 * @param display_name The display name of the user
 * @param game_id The ID of the game being played
 * @param game_name The name of the game being played
 * @param id The user ID
 * @param is_live Wether the user is live or not
 * @param thumbnail_url The template URL of the user picture
 * @param title The title of the user's stream
 * @param started_at Stream start time (empty string if offline)
 */
case class Channel(
                    broadcaster_login: String,
                    display_name: String,
                    game_id: String,
                    game_name:  String,
                    id: String,
                    is_live: Boolean,
                    // tags_ids: TODO,
                    thumbnail_url: String,
                    title: String,
                    started_at: String
                  ) {

  def getThumbnail(width: Int, height: Int): String = {
    thumbnail_url
      .replace("{width}", width.toString)
      .replace("{height}", height.toString)
  }
}

