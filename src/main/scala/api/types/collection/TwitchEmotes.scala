package api.types.collection

import api.RESTClient
import api.types.TwitchEmote
import play.api.libs.json.{Json, Reads}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * A container for the sets of twitch emotes
 *
 * @param emoticon_sets The sets of emotes which is mapping of list of
 *                      `TwitEmote`.
 */
case class TwitchEmotes(
                         emoticon_sets: List[TwitchEmote]
                       ) extends APICollection[TwitchEmote] {
  def objects: List[TwitchEmote] = emoticon_sets
  def cursor: String = "" // No pagination for this object
}

object TwitchEmotes extends APICollectionObject[TwitchEmote] {

  protected val readsObject: Reads[TwitchEmote] = Json.reads[TwitchEmote]

  /**
   * Get the emotes of a specific user.
   *
   * @param id The id of the user
   * @return A `TwitchEmotes` object.
   */
  def getEmotes(id: String): Future[TwitchEmotes] = Future {
    val result = RESTClient.getGlobalEmotes(id)
    val (emotes_ , _ ) = extractFromJson(result._1, result._2)
    new TwitchEmotes(emotes_)
  }
}
