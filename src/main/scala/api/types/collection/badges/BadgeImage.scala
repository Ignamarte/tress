package api.types.collection.badges

case class BadgeImage (
                        id: String,
                        image_url_1x: String,
                        image_url_2x: String,
                        image_url_4x: String,
                        description: String,
                        title: String,
                        click_action: Option[String],
                        click_url: Option[String],
                        isEmpty: Option[Boolean]
                      )

object BadgeImage {
  def empty: BadgeImage = BadgeImage("","","","","","",Some(""),Some(""),Some(true))
}