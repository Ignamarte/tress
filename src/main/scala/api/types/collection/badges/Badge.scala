package api.types.collection.badges

case class Badge(
                  set_id: String,
                  versions: List[BadgeImage]
                )

object Badge {
  def empty: Badge = Badge("", List())
}
