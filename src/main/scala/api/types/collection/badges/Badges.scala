package api.types.collection.badges

import api.RESTClient
import api.types.APIObject
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * A container of the sets of badges
 *
 * @param data The sets of badges which is a list of `Badge` object.
 */
case class Badges (
                    data: List[Badge]
                 )

object Badges extends APIObject[Badges] {
  implicit val badgeImageRead: Reads[BadgeImage] = Json.reads[BadgeImage]
  implicit val badgesRead: Reads[Badge] = Json.reads[Badge]
  val decoder: Reads[Badges] = Json.reads[Badges]

  /**
   * Get the badges for specific chat room
   *
   * @param id The ID of the chat room (ie: the stream id)
   * @return A `Badges` object
   */
  def getBadges(id: String): Future[Badges] = Future {
    val json = RESTClient.getChatBadges(id)._2
    extractFromJson(json)
  }

  /**
   * Get the badges common to every chat room
   *
   * @return A `Badges` object
   */
  def getGeneralBadges: Future[Badges] = Future {
    val json = RESTClient.getBadges._2
    extractFromJson(json)
  }
}