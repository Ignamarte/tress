package api.types.collection.ffz

case class FfzEmote(
                     height: Int,
                     id: Int,
                     name: String,
                     urls: Map[String, String]
                   ) {

  private val sizes = List[String]("1", "2", "4")
  /**
   * Returns an URL to the emote
   * @param size Size of the emote. Must be in `sizes`.
   * @return
   */
  def url(size: String = sizes.head): String = {
    val sizeWithDefault =
      if (sizes.contains(size)) size
      else sizes.head
    s"${urls.getOrElse(sizeWithDefault, "")}"
  }
}
