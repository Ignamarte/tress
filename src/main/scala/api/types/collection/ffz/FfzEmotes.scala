package api.types.collection.ffz

import api.RESTClient
import api.types.APIObject
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class FfzEmotes(
                     emoticons: List[FfzEmote]
                 )

object FfzEmotes extends APIObject[FfzRoom] {

  implicit val emoteRead: Reads[FfzEmote] = Json.reads[FfzEmote]
  implicit val emotesRead: Reads[FfzEmotes] = Json.reads[FfzEmotes]
  val decoder: Reads[FfzRoom] = Json.reads[FfzRoom]
  /* Cache for the emotes */
  private var globalEmotesCache: Option[Map[String, FfzEmote]] = None

  /**
   * Get FFZ emotes of a specific channel asynchronously
   *
   * @param id The ID of the channel
   * @return A Future of mapping between the emote name and the emote object
   */
  def getEmotes(id: String): Future[Map[String, FfzEmote]] = Future {
    val json = RESTClient.getFfzRoom(id)._2
    extractFromJson(json).sets
      .flatMap(e => e._2.emoticons)
      .map(emote => emote.name -> emote).toMap
  }

  /**
   * Get BTTV global emotes asynchronously. These Emotes are cached since they
   * are common to every channel and don't need to be retrieved everytime.
   *
   * @return A Future of mapping between the emote name and the emote object
   */
  def getGlobalEmotes: Future[Map[String, FfzEmote]] = Future {
    globalEmotesCache match {
      case Some(_) =>
      case None =>
        val json = RESTClient.getFfzGlobal._2
        val extractedRoom = extractFromJson(json)
        val availableSets = extractedRoom.default_sets.getOrElse(List())
          .map(set => set.toString) // Used to filter unavailable global emotes
        val emotes = extractedRoom.sets
          .filter(set => availableSets.contains(set._1))
          .flatMap(e => e._2.emoticons)
          .map(emote => emote.name -> emote)
          .toMap
        globalEmotesCache = Some(emotes)
    }
    globalEmotesCache.getOrElse(Map[String, FfzEmote]())
  }
}




