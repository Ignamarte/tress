package api.types.collection.ffz

case class FfzRoom(
                    default_sets: Option[List[Int]],
                    sets: Map[String, FfzEmotes]
                   )
