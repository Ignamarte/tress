package api.types.collection

import api.RESTClient
import api.types.User
import play.api.libs.json.{Json, Reads}

class Users(_users: List[User]) extends APICollection[User]{

  def objects: List[User] = _users
  def cursor: String = ""

  override def toString: String = _users.map(user => user.login)
    .mkString(start=" - ", sep="\n - ", end="" )
}

object Users extends APICollectionObject[User] {

  protected val readsObject: Reads[User] = Json.reads[User]

  def getAuthenticatedUser: User = {
    val result = RESTClient.getUsersDetails(None)
    val (users_ , _) = extractFromJson(result._1, result._2)
    users_.head
  }

  def getUsers(ids: List[String]): Users = {
    val result = RESTClient.getUsersDetails(Some(ids))
    val (users_ , _) = extractFromJson(result._1, result._2)
    new Users(users_.toList)
  }
}