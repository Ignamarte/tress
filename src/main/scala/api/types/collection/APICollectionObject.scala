package api.types.collection

import errors.APIError
import play.api.libs.json._
import play.api.libs.json.Reads._


trait APICollectionObject[A] {

  protected val readsObject: Reads[A]
  case class Page(cursor: Option[String])
  case class ResultWrapper(pagination: Option[Page], data: List[A])

  def extractFromJson(
                       statusCode: Int,
                       jsonString: String,
                     ): (List[A], String) = {

    implicit val readsA: Reads[A] = readsObject
    implicit val readsPage: Reads[Page] = Json.reads[Page]
    implicit val reads: Reads[ResultWrapper] = Json.reads[ResultWrapper]

    val json = Json.parse(jsonString)
    Json.fromJson[ResultWrapper](json) match {
      case JsSuccess(r, _) =>
        val cursor = r.pagination.getOrElse(Page(Some(""))).cursor.getOrElse("")
        (r.data, cursor)
      case e: JsError => throw APIError(422, e.toString)
    }
  }
}
