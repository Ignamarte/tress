package api.types.collection

import api.RESTClient
import api.types.{Channel, Pagination}
import play.api.libs.json.{Json, Reads}

/**
 * A list of Channel objects (~=users)
 */
class Channels(_channels: List[Channel], _cursor: String) {

  def objects: List[Channel] = _channels
  def cursor: String = _cursor

}

/**
 *  Companion object for the Channels class.
 *  Provides public methods to create new instances of Channels by requesting
 *  the twitch API.
 */
object Channels extends APICollectionObject[Channel] {

  // Used for parsing the json returned by the API
  protected val readsObject: Reads[Channel] = Json.reads[Channel]

  /**
   * Search for a list channels based on a query
   *
   * @param query The query
   * @param page An optional Pagination object to explore the list
   * @return A list of Channels
   */
  def search(query: String, page: Option[Pagination] = None): Channels = {
    val result = RESTClient.searchChannels(query, page)
    val (channels_, cursor_) = extractFromJson(result._1, result._2)
    new Channels(channels_, cursor_)
  }
}