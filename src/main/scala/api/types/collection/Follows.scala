package api.types.collection

import api.RESTClient
import api.types.{Follow, Pagination, User}
import play.api.libs.json.{Json, Reads}

class Follows(_follows: List[Follow], _cursor: String)
  extends APICollection[Follow]{

  def objects: List[Follow] = _follows
  def cursor: String = _cursor

  override def toString: String = _follows.map(follow => follow.broadcaster_name)
    .mkString(start=" - ", sep="\n - ", end="" )

  /*
   * Get streams associated to the follow objects
   */
  def getStreams(
                  page: Option[Pagination] = None,
                  objects: List[Follow] = this.objects
                ): Streams = {
    val (objectsHead, objectsTail)  = objects.splitAt(100)
    // If objectsHead is empty we don't need to send a request, otherwise we
    // would retrieve the top streams.
    val streams: Streams = objectsHead match {
      case List() => new Streams(List(), "")
      case _ => Streams.topStreams(
        page = page,
        users = Some(objectsHead.map(f => f.broadcaster_id))
      )
    }
    objectsTail match {
      case List() => streams
      case obj: List[Follow] =>
        new Streams(streams.objects ::: getStreams(objects=obj).objects, "")
    }
  }

  /*
   * Get users associated to the follow objects
   */
  def getUsers(
                page: Option[Pagination] = None,
                users: List[User] = List(),
                follows: List[Follow] = _follows): Users = {
    Users.getUsers(ids = objects.map(f => f.broadcaster_id))
  }
}

object Follows extends APICollectionObject[Follow] {

  protected val readsObject: Reads[Follow] = Json.reads[Follow]

  @scala.annotation.tailrec
  def getSubscriptions(
                        follows: List[Follow] = List(),
                        page: Option[Pagination] = None): Follows =  {
    val user = User.authenticatedUser
    val result = RESTClient.getSubscriptions(user.id, page)
    val parsedResult = Follows.extractFromJson(result._1, result._2)

    parsedResult match {
      case (List(), cursor_) => new Follows(follows, cursor_)
      case (_follows, "") => new Follows(follows ::: _follows, _cursor = "")
      case (_follows, cursor_) => getSubscriptions(
        follows ::: _follows,Some(Pagination("after", cursor_))
      )
    }
  }

}
