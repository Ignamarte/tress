package api.types.collection

import api.RESTClient
import api.types.{Game, Pagination}
import errors.APIError
import play.api.libs.json._

class Games(_games: List[Game], _cursor: String) extends APICollection[Game]{

  def objects: List[Game] = _games
  def cursor: String = _cursor

  override def toString: String = _games.map(stream => stream.name)
    .mkString(start=" - ", sep="\n - ", end="" )
}

object Games extends APICollectionObject[Game] {

  protected val readsObject: Reads[Game] = Json.reads[Game]


  def topGames(page: Option[Pagination] = None): Games = {
    val result = RESTClient.getTopGames(page)
    val (games_ , cursor_) = extractFromJson(result._1, result._2)
    new Games(games_, cursor_)
  }

  /**
    * Send a search query to the API to retrieve a list of games based on the
    * query string provided.
    *
    * @param query The search query string
    * @return A `Games` object
    */
  def search(query:String, page: Option[Pagination] = None): Games = {
    val result = RESTClient.searchGames(query, page)
    val (games_ , cursor_) = extractFromJson(result._1, result._2)
    new Games(games_, cursor_)
  }

}