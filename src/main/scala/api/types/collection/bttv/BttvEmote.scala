package api.types.collection.bttv

import util.config.{APIConfig, UserConfig}

case class BttvEmote(
                    id: String,
                    code: String,
                    imageType: String
                    ) {

  private val sizes = List[String]("1x", "2x", "3x")

  private lazy val baseURLStatic = APIConfig.getBttvEmoteUrlStatic
  private lazy val baseURLDynamic = APIConfig.getBttvEmoteUrl

  def url(forceGif: Boolean = false, size: String = "1x"): String = {
    val sizeWithDefault =
      if (sizes.contains(size)) size
      else sizes.head
    if (!forceGif && (imageType != "gif" || UserConfig.getDisableGifEmotes))
      baseURLStatic.replace("{{id}}", id).replace("{{size}}", sizeWithDefault)
    else
      baseURLDynamic.replace("{{id}}", id).replace("{{size}}", sizeWithDefault)
  }
}
