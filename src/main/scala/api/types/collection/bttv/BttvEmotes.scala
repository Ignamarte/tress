package api.types.collection.bttv

import api.RESTClient
import api.types.APIObject
import api.types.collection.APICollection
import play.api.libs.json
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class BttvEmotes(emotes: List[BttvEmote])

object BttvEmotes extends APIObject[BttvEmotes] {

  implicit val decoderEmote: Reads[BttvEmote] = Json.reads[BttvEmote]
  val decoder: Reads[BttvEmotes] = Json.reads[BttvEmotes]
  /* Cache for the emotes */
  private var globalEmotesCache: Option[Map[String, BttvEmote]] = None

  /**
   * Get BTTV emotes of a specific channel asynchronously
   *
   * @param id The ID of the channel
   * @return A Future of mapping between the emote name and the emote object
   */
  def getEmotes(id: String): Future[Map[String, BttvEmote]] = Future {
    val json = RESTClient.getBttvRoom(id)._2
    extractFromJson(json).emotes.map(emote => emote.code -> emote).toMap
  }

  /**
   * Get BTTV global emote sasynchronously. These Emotes are cached since they
   * are common to every channel and don't need to be retrieved everytime.
   *
   * @return A Future of mapping between the emote name and the emote object
   */
  def getGlobalEmotes: Future[Map[String, BttvEmote]] = Future {
    globalEmotesCache match {
      case Some(map) =>
      case None =>
        val json = RESTClient.getBttvGlobal._2
        val emotes = extractFromJson(json).emotes.map(
          emote => emote.code -> emote
        ).toMap
        globalEmotesCache = Some(emotes)
    }
    globalEmotesCache.getOrElse(Map[String, BttvEmote]())
  }
}






