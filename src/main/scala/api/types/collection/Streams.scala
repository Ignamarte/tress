package api.types.collection

import api.RESTClient
import api.types.{Pagination, Stream}
import errors.APIError
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}

/** A list of streams objects
  *
  * @param _streams A list of streams objects
  * @param _cursor The current page provided by the twitch API
  */
class Streams(_streams: List[Stream], _cursor: String)
  extends APICollection[Stream]{

  def objects: List[Stream] = _streams
  def cursor: String = _cursor

  def withUser(): Streams = {
    val userIDs = objects.map(s => s.user_id)
    val users = Users.getUsers(userIDs).objects
    val mapping = users.iterator.map(u => u.id -> u.login).toMap
    new Streams(
      objects.map(o => o.copy(user_login = mapping.get(o.user_id))),
      cursor
    )
  }

  override def toString: String = _streams.map(stream => stream.title)
    .mkString(start=" - ", sep="\n - ", end="" )
}

/**
  *  Companion object for the Streams class.
  *  Provides public methods to create new instances of Streams by requesting
  *  the twitch API.
  */
object Streams extends APICollectionObject[Stream] {

  // Used for parsing the json returned by the API
  protected val readsObject: Reads[Stream] = Json.reads[Stream]

  /** Return the top most popular streams.
    * Explore the list of streams using the Pagination argument
    *
    * @param page An optional Pagination object to explore the list
    * @param gameID An optional game ID to restrict the results to a single
    *               game.
    * @param users An optional list of user IDs to filter on
    * @return Return a Streams objects
    */
  def topStreams(
                  page: Option[Pagination] = None,
                  gameID: Option[String] = None,
                  users: Option[List[String]] = None

                ): Streams = {
    val result = RESTClient.getTopStreams(gameID, page, users)
    val (streams_ , cursor_) = extractFromJson(result._1, result._2)
    new Streams(streams_, cursor_)
  }

  def getStream(id: String): Option[Stream] = {
    topStreams(users = Some(List(id))).objects.headOption
  }

  def getStreams(ids: List[String]): Streams = {
    topStreams(users = Some(ids))
  }
}
