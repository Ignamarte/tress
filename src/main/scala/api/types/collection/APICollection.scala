package api.types.collection

trait APICollection[A] {
  def objects: List[A]
  def cursor: String
}
