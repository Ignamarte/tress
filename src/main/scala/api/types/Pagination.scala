package api.types

import play.api.libs.json.{Json, Reads}

case class Pagination(page: String, cursor: String) {
  require(List("after", "before").contains(page))
}

object Pagination {
  implicit val reads: Reads[Pagination] = Json.reads[Pagination]
}
