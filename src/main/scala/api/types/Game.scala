package api.types

import api.RESTClient
import api.types.collection.Games

import scala.collection.mutable

case class Game(
               id: String,
               name: String,
               box_art_url: String,
               ) {
  def getThumbnail(width: Int, height: Int): String = {
    this.box_art_url
      // Workaround for https://github.com/twitchdev/issues/issues/329
      .replace("52x72", "{width}x{height}")
      .replace("{width}", width.toString)
      .replace("{height}", height.toString)
  }
}

object Game {
  def getByID(id: String): Game = {
    val (status, json) = RESTClient.getGames(List(id), None)
    Games.extractFromJson(status, json)._1.head
  }
}
