package api.types

import scala.collection.mutable

case class Stream(
                   id: String,
                   user_id: String,
                   user_name: String,
                   game_id: String,
                   title: String,
                   `type`: String,
                   viewer_count: Int,
                   started_at: String,
                   language: String,
                   thumbnail_url: String,
                   user_login: Option[String]
                 ) {

  def chatURL: String = {
    f"https://www.twitch.tv/popout/${user_login.getOrElse("")}/chat"
  }

  def getThumbnail(width: Int, height: Int): String = {
    thumbnail_url
      .replace("{width}", width.toString)
      .replace("{height}", height.toString)
  }

  def getGameName: String = {
    val cachedValue = Stream.idsCache.get(game_id)
    if (cachedValue.isDefined) {
      cachedValue.get
    } else {
      val game = Game.getByID(this.game_id)
      Stream.idsCache(game_id) = game.name
      game.name
    }
  }
}

object Stream {
  private val idsCache: mutable.Map[String, String] = mutable.Map[String, String]()
}


