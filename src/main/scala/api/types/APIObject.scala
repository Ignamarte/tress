package api.types

import errors.APIError
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}

/**
 * A a generic trait for an object that can be extracted from json parsing.
 * It provides the method `extractFromJson` to extract the object from a json
 * string.
 *
 * Requires a json `decoder` since it can't be generated from a generic type at
 * compile time.
 *
 * @tparam A The object to extract
 */
trait APIObject[A] {

  protected val decoder: Reads[A]

  def extractFromJson(
                       jsonString: String,
                     ): A = {

    implicit val testFormat: Reads[A] = decoder

    val json = Json.parse(jsonString)

    Json.fromJson[A](json) match {
      case JsSuccess(r, _) => r
      case e: JsError => throw APIError(
        422, f"Errors: ${JsError toJson e}"
      )
    }
  }
}
