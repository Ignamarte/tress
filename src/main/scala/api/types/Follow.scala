package api.types

import api.RESTClient
import api.types.collection.Follows
import play.api.libs.json.{Json, Reads}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * A follow object as returned by the twitch API
 *
 * @param broadcaster_id   The ID of the user being followed
 * @param broadcaster_name The name of the user being followed
 * @param followed_at      Date of follow
 */
case class Follow(
                   broadcaster_id: String,
                   broadcaster_name: String,
                   followed_at: String
                  )

object Follow {
  /**
   * Retrieve the follow relationship between two users
   * @param from_id The ID of the user following
   * @param to_id The ID of the user being followed
   * @return A Follow object if `from_id` follows `to_id`, `None` otherwise
   */
  def getSubscription(from_id: String, to_id: String): Future[Option[Follow]] =
    Future {
      val (status, json) = RESTClient.getSubscription(from_id, to_id)
      val followsList = Follows.extractFromJson(status, json)._1
      followsList.headOption
  }
}
