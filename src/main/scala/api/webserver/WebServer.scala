package api.webserver

import HTML.tokenExtractor
import api.RESTClient
import util.LoginManager
import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.headers.`Content-Type`
import org.http4s.implicits._
import org.http4s.server.blaze._

import scala.concurrent.ExecutionContext.global

object WebServer {

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)
  // Dirty trick to be able to kill the server
  private var fiber: Fiber[IO, Nothing] = _

  private val app = HttpRoutes.of[IO] {
    case GET -> Root / "token" / token =>
      RESTClient.setToken(token)
      LoginManager.applyCallback()
      Ok()
    case GET -> Root =>
      Ok(tokenExtractor).map(_.withContentType(`Content-Type`(MediaType.text.html)))
  }.orNotFound

  private val server =
    BlazeServerBuilder[IO](global).bindHttp(33245, "localhost").withHttpApp(app).resource

  def init(): Unit = {
    fiber = server.use(_ => IO.never).start.unsafeRunSync()
  }

  def stop(): Unit = {
    if (fiber != null) fiber.cancel.unsafeRunSync()
  }
}