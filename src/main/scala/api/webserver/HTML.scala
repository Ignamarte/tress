package api.webserver

object HTML {
  val tokenExtractor: String = """
<html>
<style>.hidden{display: None}</style>
<div id="processing">Please wait ...</div>
<div id="success" class="hidden">Success, you can close this page !</div>
<div id="failure" class="hidden">
  Hmmm, something went wrong, please try again ...
</div>
<script>
  var match = /access_token=([^&]+)/g.exec(location.hash);
  var token = match[1];

  var req = new XMLHttpRequest();
  req.open( "GET", "/token/" + token, true );

  req.addEventListener( "readystatechange", function() {
    if (req.readyState !== XMLHttpRequest.DONE) { return; }
    document.getElementById("processing").style.display="None"
    if (req.status === 200) {
      document.getElementById("success").style.display="Block"
    } else {
      document.getElementById("failure").style.display="Block"
    };
  }, false );

  req.send()
</script>
</html>
"""
}
